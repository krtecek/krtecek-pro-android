package cz.salikovi.krtecek.krteceksms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SmsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SmsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SmsFragment extends Fragment implements
        View.OnClickListener,
        AdapterView.OnItemSelectedListener
{
    /*
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    */

    private static final int PICK_CONTACT = 1;

    private KrtecekSmsApplication myApp = null;
    private MainActivity myAct = null;

    private OnFragmentInteractionListener mListener;
    //private ArrayAdapter<clsKontakt> daKontakty;
    private clsKontaktyAdapter daKontakty;
    //private ArrayAdapter<clsSkript> daSkripty;
    private clsSkriptyAdapter daSkripty;

    public static SmsFragment newInstance(KrtecekSmsApplication app, MainActivity act /*String param1, String param2*/) {
        SmsFragment fragment = new SmsFragment();
        /*
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        */
        fragment.myApp = app;
        fragment.myAct = act;
        fragment.InitAdapter();

        return fragment;
    }

    public SmsFragment() {
        // Required empty public constructor
    }

    private void InitAdapter() {
        Context c = myAct.getApplicationContext();
        Resources res = myAct.getResources();

        //vytvorim adapter pro combo kontaktu
        //daKontakty = new ArrayAdapter<clsKontakt>(c, android.R.layout.simple_spinner_item, myApp.getKontakty());
        //daKontakty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daKontakty = new clsKontaktyAdapter(myAct, (ArrayList)myApp.getKontakty(), res);

        //vytvorim adapter pro combo skriptu
        //daSkripty = new ArrayAdapter<clsSkript>(c, android.R.layout.simple_spinner_item, myApp.getSkripty());
        //daSkripty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daSkripty = new clsSkriptyAdapter(myAct, (ArrayList)myApp.getSkripty(), res);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            /*
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            */
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_sms, container, false);

        // propojim object list kontaktu s combem (spinnerem)
        Spinner _KontaktySpinner = (Spinner) fragView.findViewById(R.id.spinnerKontakt);
        if (_KontaktySpinner != null) {
            _KontaktySpinner.setAdapter(daKontakty);
            _KontaktySpinner.setOnItemSelectedListener(this);
        }

        // propojim object list skriptu s combem (spinnerem)
        Spinner _SkriptySpinner = (Spinner) fragView.findViewById(R.id.spinnerSkript);
        if (_SkriptySpinner != null) {
            _SkriptySpinner.setAdapter(daSkripty);
            _SkriptySpinner.setOnItemSelectedListener(this);
        }

        // napojim listener na cudlik pro vyber kontaktu
        ImageButton _KontaktyImageButton = (ImageButton) fragView.findViewById(R.id.imageButtonKontakty);
        if (_KontaktyImageButton != null) {
            _KontaktyImageButton.setOnClickListener(this);
        }

        // napojim listener na cudlik odeslani zpravy
        Button _SendButton = (Button) fragView.findViewById(R.id.buttonSend);
        if (_SendButton != null) {
            _SendButton.setOnClickListener(this);
        }

        return fragView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSmsFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButtonKontakty:
                vyberKontakt();
                break;
            case R.id.buttonSend:
                odesliZpravu();
                break;
            default:
                break;
        }
    }

    public void vyberKontakt() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean Refresh() {
        boolean result = false;
        try {
            View v = getView();

            try {
                // update spinneru kontaktu
                Spinner _KontaktySpinner = (Spinner) v.findViewById(R.id.spinnerKontakt);
                if (_KontaktySpinner != null) {
                    _KontaktySpinner.setSelection(_KontaktySpinner.getSelectedItemPosition());
                }

                // update spinneru skriptu
                Spinner _SkriptySpinner = (Spinner) v.findViewById(R.id.spinnerSkript);
                if (_SkriptySpinner != null) {
                    _SkriptySpinner.setSelection(_SkriptySpinner.getSelectedItemPosition());
                }
            } catch (Exception e) {
                Log.d("RefreshSmsFrag - Error", e.toString());
            }

            daKontakty.notifyDataSetChanged();
            daSkripty.notifyDataSetChanged();

            result = true;

        } catch (Exception e) {
            result = false;
            Log.d("RefreshSmsFrag - Error", e.toString());
        } finally {
            return result;
        }
    }

    public boolean setSmsText(String txt) {
        boolean result = false;
        try {
            View v = getView();

            // nastavim text smsky
            EditText _TextSms = (EditText) v.findViewById(R.id.editTextSms);
            if (_TextSms != null) {
                _TextSms.setText(txt);
            }

            result = true;

        } catch (Exception e) {
            result = false;
            Log.d("setSmsText - Error", e.toString());
        } finally {
            return result;
        }
    }

    public void odesliZpravu() {
        View v = getView();
        clsDb db = myApp.getDb();
        Context c = myApp.getApplicationContext();

        try {
            Spinner kontakt = (Spinner) v.findViewById(R.id.spinnerKontakt);
            Spinner skript = (Spinner) v.findViewById(R.id.spinnerSkript);
            EditText cislo = (EditText) v.findViewById(R.id.editTextPrijemce);
            EditText text = (EditText) v.findViewById(R.id.editTextSms);

            if ((kontakt != null) && (skript != null) && (cislo != null) && (text != null)) {

                clsZprava msg = new clsZprava();
                String txt = text.getText().toString().trim();
                clsKontakt kon = (clsKontakt) kontakt.getSelectedItem();
                clsSkript skr = (clsSkript) skript.getSelectedItem();

                //musim mit zadany text
                if (txt.length() > 0) {
                    msg.sms = txt;

                    if (kon.SrvDbId != -1) {

                        //mam vybrany kontakt, vic nepotrebuju
                        msg.kontakt_id = kon.SrvDbId;

                    } else if ((skr.SrvDbId != -1) && (cislo.length() >= 6)) {

                        //mam skript a cislo
                        msg.skript_id = skr.SrvDbId;
                        msg.cislo = cislo.getText().toString();

                    } else if (cislo.length() >= 6) {

                        //mam cislo a skript si urci automaticky server krtecka
                        msg.cislo = cislo.getText().toString();

                    } else {
                        //nemam nic co potrebuju, zprava nebude
                        Toast.makeText(c, R.string.cSEND_MUSIS_ZADAT_PRIJEMCE, Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(c, R.string.cSEND_MUSIS_ZADAT_TEXT, Toast.LENGTH_SHORT).show();
                    return;
                }

                myAct.myComThread.sendMessage(msg);
                text.setText("");
            }
        } catch (Exception e) {
            Log.d("SendSms", e.toString());
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {

            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri uriContact = data.getData();

                    String contactID = "";
                    String contactName = "";
                    String contactNumber = "";

                    //zjistim ID kontaktu a jmeno
                    String[] clmnGroupID = new String[] {
                            ContactsContract.Contacts._ID,
                            ContactsContract.Contacts.DISPLAY_NAME
                    };
                    Cursor cursorID = myAct.getContentResolver().query(uriContact, clmnGroupID, null, null, null);
                    if (cursorID.moveToFirst()) {
                        contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
                        contactName = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    }
                    cursorID.close();
                    Log.d("PickContact", "Contact ID: " + contactID);
                    Log.d("PickContact", "Contact DISPLAY_NAME: " + contactName);

                    //zjistim telefonni cislo kontaktu
                    String[] clmnGroupNumber = new String[] {
                            ContactsContract.CommonDataKinds.Phone.NUMBER
                    };
                    String qryWhere = "(" + ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?)";
                    String[] clmnGroupContatcID = new String[] {
                            contactID
                    };
                    Cursor cursorPhone = myAct.getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            clmnGroupNumber,
                            qryWhere,
                            clmnGroupContatcID,
                            null
                    );
                    if (cursorPhone.moveToFirst()) {
                        contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    cursorPhone.close();

                    //odstranim nechtene znaky v cisle
                    contactNumber = contactNumber.replace("-", "");
                    View v = getView();
                    try {
                        EditText _CisloPrijemceEdit = (EditText) v.findViewById(R.id.editTextPrijemce);
                        if (_CisloPrijemceEdit != null) {
                            _CisloPrijemceEdit.setText(contactNumber);
                        }
                    } catch (Exception e) {
                        Log.d("PickContact", e.toString());
                    }
                } //end -> result=ok
                break;

        } //end - switch
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        View v = getView();
        try {
            switch (parent.getId()) {

                case R.id.spinnerKontakt:
                    Spinner _SkriptySpinner = (Spinner) v.findViewById(R.id.spinnerSkript);
                    if (_SkriptySpinner != null) {
                        //kdyz jsem nevybral kontakt (0 = "vyber kontakt"), umoznim vybirat skript
                        _SkriptySpinner.setEnabled(position == 0);
                    }
                    EditText _CisloPrijemceEdit = (EditText) v.findViewById(R.id.editTextPrijemce);
                    if (_CisloPrijemceEdit != null) {
                        //kdyz jsem nevybral kontakt (0 = "vyber kontakt"), umoznim zadat cislo
                        _CisloPrijemceEdit.setEnabled(position == 0);
                    }
                    break;

                case R.id.spinnerSkript:
                    Spinner _KontaktySpinner = (Spinner) v.findViewById(R.id.spinnerKontakt);
                    if (_KontaktySpinner != null) {
                        //kdyz jsem nevybral skript (0 = "vyberte skript"), umoznim vybirat kontakt
                        _KontaktySpinner.setEnabled(position == 0);
                    }
                    break;

                default:
                    break;

            }   //end switch
        } catch (Exception e) {
            Log.d("SpinnerSelectItem", e.toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //nic se nevybralo, nic nedelam
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onSmsFragmentInteraction(Uri uri);
    }

}
