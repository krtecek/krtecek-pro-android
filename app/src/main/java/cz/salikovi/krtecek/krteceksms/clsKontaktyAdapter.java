package cz.salikovi.krtecek.krteceksms;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salik on 17. 3. 2016.
 */
public class clsKontaktyAdapter extends BaseAdapter {

    private Activity act;
    private List<clsKontakt> data;
    private LayoutInflater inflater = null;

    public Resources res;

    public clsKontaktyAdapter (Activity a, ArrayList d, Resources r) {
        act = a;
        data = d;
        res = r;

        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //trida pro radek ListView
    public static class ViewHolder{
        public ImageView kontaktAvatar;
        public TextView kontaktPrezdivka;
    }

    @Override
    public int getCount() {
        if (data.size() <= 0) {
            return 0;
        } else {
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        clsKontakt con = null;
        try {
            con = data.get(position);
        } catch (Exception e) {
            con = null;
        } finally {
            return con;
        }
    }

    @Override
    public long getItemId(int position) {
        int id = -1;
        try {
            clsKontakt con = data.get(position);
            id = con.SrvDbId;
        } catch (Exception e) {
            id  = -1;
        } finally {
            return id;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, false);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, true);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent, boolean dropDown) {
        View row = convertView;
        ViewHolder holder;
        clsKontakt con = null;

        if (row == null) {
            if (dropDown) {
                row = inflater.inflate(R.layout.kontakt_combo_item_drop_down, null);
            } else {
                row = inflater.inflate(R.layout.kontakt_combo_item, null);
            }

            holder = new ViewHolder();
            holder.kontaktAvatar = (ImageView) row.findViewById(R.id.imageViewAvatar);
            holder.kontaktPrezdivka = (TextView) row.findViewById(R.id.textViewPrezdivka);

            row.setTag( holder );    //set holder with LayoutInflater
        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (data.size() <= 0) {
            holder.kontaktAvatar.setImageResource(android.R.color.transparent);
            holder.kontaktPrezdivka.setText("Žádná data");
        } else {
            con = data.get(position);
            if (con.Avatar != null) {
                holder.kontaktAvatar.setImageBitmap(con.Avatar);
            } else {
                holder.kontaktAvatar.setImageResource(android.R.color.transparent);
            }
            holder.kontaktPrezdivka.setText(con.toString());
        }

        return row;
    }
}
