package cz.salikovi.krtecek.krteceksms;

/**
 * Created by salik on 14. 7. 2015.
 */

import android.app.Application;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class KrtecekSmsApplication extends Application {

    private List<clsKontakt> kontakty = null;
    private List<clsSkript> skripty = null;
    private List<clsZprava> fronta = null;
    private List<clsZprava> historie = null;

    private clsComunicationThread thr = null;
    private clsSrvNastaveni srvnastaveni = null;
    private clsDb db = null;

    public clsSrvNastaveni getSrvNastaveni() {
        return srvnastaveni;
    }
    public List<clsKontakt> getKontakty() {
        return kontakty;
    }
    public List<clsSkript> getSkripty() {
        return skripty;
    }
    public List<clsZprava> getFronta() {
        return fronta;
    }
    public List<clsZprava> getHistorie() {
        return historie;
    }
    public clsDb getDb() {
        return db;
    }
    public void setDb(clsDb db) {
        this.db = db;
    }
    public clsComunicationThread getThr() {
        return thr;
    }
    public void setThr(clsComunicationThread thr) {
        this.thr = thr;
    }

    public KrtecekSmsApplication() {

        srvnastaveni = new clsSrvNastaveni();
        kontakty = new ArrayList<clsKontakt>();
        skripty = new ArrayList<clsSkript>();
        fronta = new ArrayList<clsZprava>();
        historie = new ArrayList<clsZprava>();

    }

    @Override
    public void onTerminate() {
        thr.quit();
        thr = null;

        super.onTerminate();
    }

    public boolean NactiSrvNastaveni() {
        boolean result = false;
        try {
            srvnastaveni.AdresaKrtekServeru = db.readStrSettings("SrvURL", "https://localhost:port");
            srvnastaveni.Login = db.readStrSettings("SrvLogin", "KrtekLogin");
            srvnastaveni.Heslo = db.readStrSettings("SrvPass", "KrtekHeslo");
            result = true;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public boolean UlozSrvNastaveni() {
        try {
            db.writeStrSettings("SrvURL", srvnastaveni.AdresaKrtekServeru);
            db.writeStrSettings("SrvLogin", srvnastaveni.Login);
            db.writeStrSettings("SrvPass", srvnastaveni.Heslo);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
