package cz.salikovi.krtecek.krteceksms;

/**
 * Created by salik on 7. 3. 2016.
 */
public enum clsTypAkce {
    taUNDEFINED,
    taLOOP,
    taREFRESH_QUEUE,
    taREFRESH_HISTORY,
    taSYNCHRONIZE_DATA,
    taSEND_SMS,
    taPAUSE_SMS,
    taRESUME_SMS,
    taDELETE_QUEUE_SMS,
    taDELETE_HISTORY_SMS,
    taARCHIVE_SMS,
    taUNARCHIVE_SMS,
    taSEND_UNSENDED_SMS,
    taCHECK_QUEUE_CHANGES,
    taSHOW_PROGRESS_DLG,
    taUPDATE_PROGRESS_DLG,
    taHIDE_PROGRESS_DLG,
    taTEST_SETTINGS,
    taREFRESH_SCRIPTS,
    taREFRESH_CONTACTS,
    taREFRESH_MESSAGES,
    taSHOW_POPUP_INFO
}
