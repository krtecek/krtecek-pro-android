package cz.salikovi.krtecek.krteceksms;

import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by salik on 24. 7. 2015.
 */
public class clsHttpHeader  {
    public Map<String, List<String>> data;

    public clsHttpHeader() {
        data = null;
    }

    public String getFormatedHeaderData(String lineEnd) {
        String result = "";

        if (data != null) {
            for (Map.Entry<String, List<String>> entry : data.entrySet()) {
                if ( entry.getKey().isEmpty() ) {
                    result = result + entry.getValue().get(0) + lineEnd;
                } else {
                    result = result + entry.getKey() + ": " + entry.getValue().get(0) + lineEnd;
                }
            }
        }

        return result;
    }
    public String getFormatedHeaderData() {
        return getFormatedHeaderData("\n");
    }

    public boolean clear() {
        boolean result = false;
        try {
            if (data != null) {
                data.clear();
            }
            result = true;
        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }
    }

    public boolean loadFromConnection(HttpURLConnection con) {
        boolean result = false;
        try {
            clear();
            data = con.getHeaderFields();
            result = true;
        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }
    }

    public String getHeaderValue(String fieldName) {
        String result = "";
        try {
            if (data != null) {

                if (data.containsKey(fieldName)) {
                    List<String> listVal = data.get(fieldName);
                    Iterator iter = listVal.iterator();

                    while (iter.hasNext()) {
                        String val = (String) iter.next();
                        result = result + " " + val;
                    }
                }

            }
        } catch (Exception e) {
            result = "";
            Log.d("getHeaderValue(" + fieldName + ") - error", e.toString());
        } finally {
            return result.trim();
        }

    }

    public String getCharset() {
        String result = "";
        try {

            String charset = getHeaderValue("Content-Type").toUpperCase();
            if (charset.contains("CHARSET=")) {
                String[] arr = charset.split("charset=");
                if (arr.length > 1) {
                    result = arr[1];
                }
            }

        } catch (Exception e) {
            result = "";
            Log.d("getCharset - error", e.toString());
        } finally {
            return result;
        }
    }
}
