package cz.salikovi.krtecek.krteceksms;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

/**
 * Created by salik on 14. 7. 2015.
 */

public class clsKontakt {

    public Integer SrvDbId;
    public String Prezdivka;
    public String Cislo;
    public Integer SrvScrDbId;
    public Bitmap Avatar;
    public String updTsStr;

    public clsKontakt() {
        SrvDbId = -1;
        Prezdivka = "";
        Cislo = "";
        SrvScrDbId = -1;
        Avatar = null;
        updTsStr = "";
    }

    public clsKontakt(Integer aSrvDbId, String aPrezdivka, String aCislo, Integer aSrvScrDbId, Bitmap aAvatar, String aUpdTsStr) {
        SrvDbId = aSrvDbId;
        Prezdivka = aPrezdivka;
        Cislo = aCislo;
        SrvScrDbId = aSrvScrDbId;
        Avatar = aAvatar;
        updTsStr = aUpdTsStr;
    }

    @Override
    public String toString() {
        String result;
        if (this.Cislo == "") {
            result = String.format("%s", this.Prezdivka);
        } else {
            result = String.format("%s (%s)", this.Prezdivka, this.Cislo);
        }
        return result;
    }

    public void setAvatar(String bmpStrStream) {

    }
}
