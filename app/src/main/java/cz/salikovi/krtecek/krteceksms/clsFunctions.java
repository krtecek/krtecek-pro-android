package cz.salikovi.krtecek.krteceksms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by salik on 28. 7. 2015.
 */
public class clsFunctions {

    public static Date str2date(String date, String date_format) {
        Date result = null;

        if (date_format.equals("")) {
            date_format = "yyyy-MM-dd HH:mm:ss";
        }
        try {

            SimpleDateFormat formatter = new SimpleDateFormat(date_format);
            result = formatter.parse(date);

        } catch (Exception e) {
            result = new Date();
            Log.d("str2date", "(" + date + " - " + date_format + ") - " + e.toString());
        } finally {
            return result;
        }
    }

    public static Date str2date(String date) {
        return str2date(date, "");
    }

    public static String NazevStavu(clsStavZpravy stav) {
        String result = "";
        switch (stav) {
            case szUNKNOWN:
                result = "?";
                break;
            case szWAITING:
                result = "čeká na odeslání";
                break;
            case szSENDING:
                result = "odesílá se";
                break;
            case szPAUSED:
                result = "pozastavena";
                break;
            case szEDITED:
                result = "edituje se";
                break;
            case szERROR:
                result = "chyba odesílání";
                break;
            case szSENDED:
                result = "odeslána";
                break;
            default:
                result = "?";
        }
        return result;
    }

    public static int ResImgStavu(clsStavZpravy stav) {
        int result;
        switch (stav) {
            case szUNKNOWN:
                result = R.drawable.qmark_icon_16;
                break;
            case szWAITING:
                result = R.drawable.wait_icon_16;
                break;
            case szSENDING:
                result = R.drawable.play_icon_16;
                break;
            case szPAUSED:
                result = R.drawable.pause_icon_16;
                break;
            case szEDITED:
                result = R.drawable.edit_icon_16;
                break;
            case szERROR:
                result = R.drawable.error_16;
                break;
            case szSENDED:
                result = R.drawable.ok_16;
                break;
            default:
                result = R.drawable.qmark_icon_16;
        }
        return result;
    }

    public static clsStavZpravy int2stav(int stav) {
        clsStavZpravy result;
        switch (stav) {
            case 0:
                result = clsStavZpravy.szUNKNOWN;
                break;
            case 1:
                result = clsStavZpravy.szWAITING;
                break;
            case 2:
                result = clsStavZpravy.szSENDING;
                break;
            case 3:
                result = clsStavZpravy.szPAUSED;
                break;
            case 4:
                result = clsStavZpravy.szEDITED;
                break;
            case 5:
                result = clsStavZpravy.szERROR;
                break;
            case 6:
                result = clsStavZpravy.szSENDED;
                break;
            default:
                result = clsStavZpravy.szUNKNOWN;
        }
        return result;
    }

    public static int stav2int(clsStavZpravy stav) {
        return stav.ordinal();
    }

    public static clsTypAkce int2typ (int typ) {
        clsTypAkce result = clsTypAkce.taUNDEFINED;
        try {
            result = clsTypAkce.values()[typ];
        } catch (Exception e) {
            result = clsTypAkce.taUNDEFINED;
            Log.d("int2typ", e.toString());
        } finally {
            return result;
        }
    }

    public static int typ2int(clsTypAkce typ) {
        return typ.ordinal();
    }

    public static ProgressDialog createCancelProgressDialog(Context cont, ProgressDialog pd, String title, String message, String btnText, final clsComunicationThread comThr) {
        boolean is_created = false;

        if (pd == null) {
            pd = new ProgressDialog(cont);
        } else {
            is_created = true;
        }

        //ukazatel
        pd.setMax(100); //0 - 100 %
        pd.setProgress(0);

        //popisky
        pd.setTitle(title);
        pd.setMessage(message);

        if (!is_created) {
            //tlactiko na zruseni
            pd.setCancelable(false);
            pd.setButton(DialogInterface.BUTTON_NEGATIVE, btnText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (comThr != null) {
                        comThr.prerusAkci(); //prerusi aktualni operaci vlakna
                    }
                    dialog.dismiss();
                }
            });
        }

        return pd;
    }

    public static int booleanToInt(boolean value) {
        // Convert true to 1 and false to 0.
        return value ? 1 : 0;
    }

    public static boolean intToBoolean(int value) {
        return (value == 1);
    }

    public static boolean saveTextToFile(String directory, String fileName, String text) {
        boolean result = false;
        PrintStream printStream = null;
        try {
            //acces sdcard
            File sdCard = Environment.getExternalStorageDirectory();

            //get directory
            File dir = new File(sdCard.getAbsolutePath() + "/" + directory);
            dir.mkdirs();

            //write to file
            OutputStream os = new FileOutputStream(dir.getAbsolutePath() + "/" + fileName);
            printStream = new PrintStream(os);
            printStream.print(text);
            //printStream.close();

            result = true;
        } catch (Exception e) {
            result = false;
            Log.d("saveTextToFile", e.toString());
        } finally {
            if (printStream != null) {
                printStream.close();
            }
            return result;
        }
    }

    public static boolean saveLogcatToFile(String directory, String fileName) {
        final String cCRLF = "\r\n";
        boolean result = false;
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(process.getInputStream()) );

            StringBuilder log= new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line + cCRLF);
            }

            saveTextToFile(directory, fileName, log.toString());
            clearLogcat();
        }
        catch (IOException e) {
            result = false;
            Log.d("saveLogcatToFile", e.toString());
        } finally {
            return result;
        }
    }

    public static boolean clearLogcat() {
        boolean result = false;
        try {
            Runtime.getRuntime().exec("logcat -c");
            result = true;
        } catch (Exception e) {
            result = false;
            Log.d("clearLogcat", e.toString());
        } finally {
            return result;
        }
    }

    public static Bitmap base64ToBitmap (String imgStr) {
        Bitmap result = null;
        try {
            byte[] decodedString = Base64.decode(imgStr, Base64.DEFAULT);
            result = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (Exception e) {
            result = null;
        } finally {
            return result;
        }
    }

    public static Bitmap blob2Bitmap(byte[] blob) {
        Bitmap result = null;
        try {
            result = BitmapFactory.decodeByteArray( blob, 0, blob.length );
        } catch (Exception e) {
            result = null;
        } finally {
            return result;
        }
    }

    public static byte[] bitmap2blob(Bitmap bmp) {
        byte[] result = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, bos);
            result = bos.toByteArray();
        } catch (Exception e) {
            result = null;
        } finally {
            return result;
        }
    }

}
