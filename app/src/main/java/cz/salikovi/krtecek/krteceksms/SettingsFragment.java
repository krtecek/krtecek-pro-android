package cz.salikovi.krtecek.krteceksms;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment implements
        View.OnClickListener
{
    /*
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    */

    private KrtecekSmsApplication myApp = null;
    private MainActivity myAct = null;
    private OnFragmentInteractionListener mListener;

    public static SettingsFragment newInstance(KrtecekSmsApplication app, MainActivity act /*String param1, String param2*/) {
        SettingsFragment fragment = new SettingsFragment();
        /*
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        */
        fragment.myApp = app;
        fragment.myAct = act;
        return fragment;
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            /*
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            */
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_settings, container, false);

        //predvyplnim nastaveni
        EditText _ServerEdit = (EditText) fragView.findViewById(R.id.editTextKrtekServer);
        if (_ServerEdit != null) {
            //kontrola tvaru adresy
            final String urlRegexp = "^http[s]?:\\/\\/[a-zA-Z0-9\\-_.]+[:]?[0-9]*$[^\\/]*";
            //
            InputFilter[] inFilter = new InputFilter[] {new clsServerUrlRegexpInputFilter(urlRegexp)};
            _ServerEdit.setFilters(inFilter);
            _ServerEdit.addTextChangedListener(new clsServerUrlTextChangeWatcher(_ServerEdit, urlRegexp));
            //adresa
            _ServerEdit.setText(myApp.getSrvNastaveni().AdresaKrtekServeru);
        }

        EditText _LoginEdit = (EditText) fragView.findViewById(R.id.editTextLogin);
        if (_LoginEdit != null) {
            _LoginEdit.setText(myApp.getSrvNastaveni().Login);
        }

        EditText _HesloEdit = (EditText) fragView.findViewById(R.id.editTextHeslo);
        if (_HesloEdit != null) {
            _HesloEdit.setText(myApp.getSrvNastaveni().Heslo);
        }

        // napojim listener na cudlik ulozeni nastaveni
        Button _UlozNastButton = (Button) fragView.findViewById(R.id.buttonUlozNastaveni);
        if (_UlozNastButton != null) {
            _UlozNastButton.setOnClickListener(this);
        }

        // napojim listener na cudlik testu nastaveni
        Button _TestNastButton = (Button) fragView.findViewById(R.id.buttonTestNastaveni);
        if (_TestNastButton != null) {
            _TestNastButton.setOnClickListener(this);
        }

        return fragView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSettingsFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonUlozNastaveni:
                ulozNastaveni();
                break;
            case R.id.buttonTestNastaveni:
                testNastaveni();
                break;
            default:
                break;
        }
    }

    public void ulozNastaveni() {
        clsSrvNastaveni nast = myApp.getSrvNastaveni();
        View v = getView();

        try {
            //predvyplnim nastaveni
            EditText _ServerEdit = (EditText) v.findViewById(R.id.editTextKrtekServer);
            if (_ServerEdit != null) {
                nast.AdresaKrtekServeru = _ServerEdit.getText().toString();
            }

            EditText _LoginEdit = (EditText) v.findViewById(R.id.editTextLogin);
            if (_LoginEdit != null) {
                nast.Login = _LoginEdit.getText().toString();
            }

            EditText _HesloEdit = (EditText) v.findViewById(R.id.editTextHeslo);
            if (_HesloEdit != null) {
                nast.Heslo = _HesloEdit.getText().toString();
            }

            if (myApp.UlozSrvNastaveni()) {

                //prenesu parametry do KrtekServeru
                updateSrvSettings(nast);

                Toast.makeText(myApp.getApplicationContext(), "Nastavení bylo uloženo.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(myApp.getApplicationContext(), "Nastavení se nepodařilo uložit!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.d("ulozNastaveni", e.toString());
        }
    }

    public void testNastaveni() {
        clsComunicationThread comThr = myApp.getThr();
        try {
            clsSrvNastaveni nast = myApp.getSrvNastaveni();
            comThr.testNastaveni(nast);
        } catch (Exception e) {
            Log.d("testNastaveni", e.toString());
        }
    }

    public void updateSrvSettings(clsSrvNastaveni ss) {
        clsComunicationThread comThr = myApp.getThr();
        try {
            comThr.setServerSettings(ss);
        } catch (Exception e) {
            Log.d("updateSrvSettings", e.toString());
        }
    }

    public void setSettingsResult(String tr) {
        View v = getView();

        try {
            WebView log = (WebView) v.findViewById(R.id.webViewTestNastaveni);

            log.loadDataWithBaseURL("file:///android_res/drawable/", tr, "text/html", "UTF-8", null);
            log.invalidate();

        } catch (Exception e) {
            Log.d("setSettingsResult", e.toString());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onSettingsFragmentInteraction(Uri uri);
    }

}
