package cz.salikovi.krtecek.krteceksms;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.os.Handler;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by salik on 20.8.2015.
 */

/**
 * Handler thread si v RUN spusti LOOPER, ktery pak obsluhuje zravy (nepotrebuju a nesmim RUN override-ovat jako v delphi)
 * */
public class clsComunicationThread extends HandlerThread
    implements Handler.Callback
{
    private Handler hndlr = null;
    private Handler uiHndlr = null;
    private MainActivity act = null;
    private clsDb db = null;
    private clsKrtekServer srv = null;
    private final clsStatus status;

    public Handler getHndlr() {
        return hndlr;
    }

    public clsComunicationThread(String name, clsDb db, clsSrvNastaveni srvnst, Handler uiHndlr, MainActivity act) {
        super(name);

        this.status = new clsStatus();

        this.db = db;
        this.uiHndlr = uiHndlr;
        this.act = act;

        this.srv = new clsKrtekServer(
                srvnst.AdresaKrtekServeru,
                srvnst.Login,
                srvnst.Heslo
        );

        setAkce(clsTypAkce.taUNDEFINED);
    }

    @Override
    protected void onLooperPrepared() {
        //super.onLooperPrepared();
        hndlr = new Handler(getLooper(), this);
        setAkce(clsTypAkce.taLOOP); //at vim, ze uz mam looper hotovy a nastartovany
    }

    @Override
    public boolean handleMessage(Message msg) {
        boolean result = true;
        try {
            clsTypAkce msg_type = clsFunctions.int2typ(msg.what);
            switch (msg_type) {

                case taUNDEFINED:
                    //setAkce(msg_type); //nemelo by se nidky vyvolat
                    break;
                case taLOOP:
                    //setAkce(msg_type); //nemelo by se nikdy vyvolat
                    break;
                case taREFRESH_QUEUE:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taREFRESH_HISTORY:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taSYNCHRONIZE_DATA:
                    setAkce(msg_type);
                    msg2uiShowSynchronizeProgressDlg();
                    syncServerDataInt(
                            clsFunctions.intToBoolean(msg.arg1),
                            clsFunctions.intToBoolean(msg.arg2),
                            clsFunctions.intToBoolean((int) msg.obj));
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taSEND_SMS:
                    setAkce(msg_type);
                    msg2uiShowSendSmsProgressDlg();
                    sendMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taPAUSE_SMS:
                    setAkce(msg_type);
                    msg2uiShowPauseQueueMessageProgressDlg();
                    pauseQueueMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taRESUME_SMS:
                    setAkce(msg_type);
                    msg2uiShowResumeQueueMessageProgressDlg();
                    resumeQueueMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taDELETE_QUEUE_SMS:
                    setAkce(msg_type);
                    msg2uiShowDeleteMessageProgressDlg();
                    deleteQueueMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taDELETE_HISTORY_SMS:
                    setAkce(msg_type);
                    msg2uiShowDeleteMessageProgressDlg();
                    deleteHistoryMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taARCHIVE_SMS:
                    setAkce(msg_type);
                    msg2uiShowArchiveHistoryMessageProgressDlg();
                    archiveHistoryMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taUNARCHIVE_SMS:
                    setAkce(msg_type);
                    msg2uiShowUnarchiveHistoryMessageProgressDlg();
                    unarchiveHistoryMessageInt((clsZprava) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taSEND_UNSENDED_SMS:
                    setAkce(msg_type);
                    sendUnsendedMessagesInt();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taCHECK_QUEUE_CHANGES:
                    setAkce(msg_type);
                    checkQueueChangesInt();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taSHOW_PROGRESS_DLG:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taUPDATE_PROGRESS_DLG:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taHIDE_PROGRESS_DLG:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taTEST_SETTINGS:
                    setAkce(msg_type);
                    msg2uiShowSettingTestProgressDlg();
                    testNastaveniInt((clsSrvNastaveni) msg.obj);
                    msg2uiHhideProgressDlg();
                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taREFRESH_SCRIPTS:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taREFRESH_CONTACTS:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taREFRESH_MESSAGES:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;
                case taSHOW_POPUP_INFO:
                    setAkce(msg_type);

                    setAkce(clsTypAkce.taLOOP);
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            result = false;
            Log.d("ComThr-HndlMsg", e.toString());
        }
        return result;
    }

    private void syncServerDataInt(boolean wholeHistory, boolean endQuestion, boolean reloadContScriptData) {
        boolean result = true;
        try {
            Resources res = act.getResources();

            if ( srv.ReadyForComunication() ) {

                if ( (getAkce() != clsTypAkce.taSYNCHRONIZE_DATA) || ((getAkce() == clsTypAkce.taSYNCHRONIZE_DATA) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }
                msg2uiUpdateProgressDlg(33,
                        res.getString(R.string.titleSynchronizeDataDlg),
                        res.getString(R.string.descSynchronizeDataContactsDlg)
                );
                result = refreshContactsInt(false, reloadContScriptData);


                if ( (getAkce() != clsTypAkce.taSYNCHRONIZE_DATA) || ((getAkce() == clsTypAkce.taSYNCHRONIZE_DATA) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }
                msg2uiUpdateProgressDlg(66,
                        res.getString(R.string.titleSynchronizeDataDlg),
                        res.getString(R.string.descSynchronizeDataScriptsDlg)
                );
                result = result && refreshScriptsInt(false, reloadContScriptData);


                if ( (getAkce() != clsTypAkce.taSYNCHRONIZE_DATA) || ((getAkce() == clsTypAkce.taSYNCHRONIZE_DATA) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }
                msg2uiUpdateProgressDlg(99,
                        res.getString(R.string.titleSynchronizeDataDlg),
                        res.getString(R.string.descSynchronizeDataMessagesDlg)
                );
                result = result && refreshMessages(true, wholeHistory);

            } else {
                //nejde se spojit se serverem
                result = false;

                //doplnim jen "offline" kontakty a skripty
                refreshContactsInt(true, false);
                refreshScriptsInt(true, false);
            }
        } catch (Exception e) {
            result = false;
            Log.d("syncServerData", e.toString());
        } finally {
            //zpracuju vysledek
            msg2uiSyncDataResult(result, endQuestion);
            return;
        }
    }

    private void msg2uiSyncDataResult(boolean result, boolean endQuestion) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taSYNCHRONIZE_DATA);
        msg.arg1 = clsFunctions.booleanToInt(result);
        msg.arg2 = clsFunctions.booleanToInt(endQuestion);

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiSyncDataRes", e.toString());
        }
    }

    private boolean refreshContactsInt(boolean offline, boolean refreshData) {
        boolean result = false;
        try {
            if (!offline) {
                String lastChange;
                if (!refreshData) {
                    lastChange = db.getLastContactChangeDateTimeStr();
                } else {
                    lastChange =  null;
                    db.deleteAllContacts();
                }

                int cnt_kontaktu = db.getContactCnt();
                List<clsKontakt> srv_contacts = new ArrayList<clsKontakt>();
                switch (srv.ReadContactsFromServer(cnt_kontaktu, srv_contacts, lastChange)) {
                    case 0:
                        result = false;
                        break;
                    case 1:
                        result = true;
                        break;
                    case -1:
                        db.deleteAllContacts();
                        result = true;
                        break;
                    default:
                        result = false;
                }

                //aktualizuju parametry kontaktu
                if (srv_contacts.size() > 0) {
                    db.updateContacts(srv_contacts);
                }
            }

            //nactu aktualizovane kontakty
            List<clsKontakt> contacts = new ArrayList<clsKontakt>();
            contacts.clear();
            contacts.add(new clsKontakt(-1, "<vyber kontakt>", "", -1, null, null));
            db.readContacts(contacts);
            //
            msg2uiRefreshContactsResult(contacts);

        } finally {
            return result;
        }
    }

    private void msg2uiRefreshContactsResult(List<clsKontakt> con) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taREFRESH_CONTACTS);
        msg.obj = con;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiRefCntResult", e.toString());
        }
    }

    private boolean refreshScriptsInt(boolean offline, boolean refreshData) {
        boolean result = false;
        try {
            if (!offline) {
                String lastChange;
                if (!refreshData) {
                    lastChange = db.getLastScriptChangeDateTimeStr();
                } else {
                    lastChange = null;
                    db.deleteAllScripts();
                }
                int cnt_skriptu = db.getScriptCnt();
                List<clsSkript> srv_scripts = new ArrayList<clsSkript>();
                switch (srv.ReadScriptsFromServer(cnt_skriptu, srv_scripts, lastChange)) {
                    case 0:
                        result = false;
                        break;
                    case 1:
                        result = true;
                        break;
                    case -1:
                        db.deleteAllScripts();
                        result = true;
                        break;
                    default:
                        result = false;
                }

                //aktualizuju parametry skriptu
                if (srv_scripts.size() > 0) {
                    db.updateScripts(srv_scripts);
                }
            }

            //nactu aktualizovane skripty a predam
            List<clsSkript> scripts = new ArrayList<clsSkript>();
            scripts.clear();
            scripts.add(new clsSkript(-1, "<vyber odesílací skript>", null, null));
            db.readScripts(scripts);
            //
            msg2uiRefreshScriptsResult(scripts);

        } finally {
            return result;
        }
    }

    private void msg2uiRefreshScriptsResult(List<clsSkript> scr) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taREFRESH_SCRIPTS);
        msg.obj = scr;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiRefScrResult", e.toString());
        }
    }

    private boolean refreshMessages(boolean WhooleQueue, boolean WholeSended) {
        boolean result = false;

        List<clsZprava> zpravy = new ArrayList<clsZprava>();
        int lastFrontaID = -1;
        int lastOdeslanoID = -1;

        if (!WholeSended) {
            lastOdeslanoID = db.getLastFrontaIdOdeslano();
        } else {
            db.delAllHistoryMessages();
        }

        if (WhooleQueue) {
            db.delOldQueueMessages();   //vymaze z fronty zpravy, ktere maji "fronta_id" (budu stahovat celou frontu ze serveru, tak si natahnu aktualni)
        } else {
            lastFrontaID = db.getLastFrontaIdFronta();
        }

        try {
            //#1 - odeslane
            zpravy.clear();
            if (srv.ReadSendedFromServer(zpravy, lastOdeslanoID)) {
                //mam seznam odeslanych zprav, aktualizuju data v DB programu
                if (!zpravy.isEmpty()) {

                    Iterator iter = zpravy.iterator();
                    while (iter.hasNext()) {
                        db.zpracujZpravu((clsZprava) iter.next());
                    }

                }
            }

            //#2 - fronta
            zpravy.clear();
            if (srv.ReadQueueFromServer(zpravy, lastFrontaID)) {
                //mam seznam zprav ve fronte, aktualizuju data v DB programu
                if (!zpravy.isEmpty()) {

                    Iterator iter = zpravy.iterator();
                    while (iter.hasNext()) {
                        db.zpracujZpravu((clsZprava) iter.next());
                    }

                }
            }
            msg2uiRefreshMessagesResult();

            result = true;
        } catch (Exception e) {
            result = false;
            Log.d("thr-refreshSMS", e.toString());
        } finally {
            return result;
        }
    }

    private void msg2uiRefreshMessagesResult() {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taREFRESH_MESSAGES);

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiRefSmsResult", e.toString());
        }
    }

    private void sendUnsendedMessagesInt() {
        boolean result = false;
        int snd_cnt = 0;
        try{

            clsZprava msg = null;
            List<clsZprava> msgs = db.getUnsendedMessages();
            if (msgs.size() > 0) {
                Iterator iter = msgs.iterator();
                while (iter.hasNext()) {

                    //vycucnu zpravu
                    msg = (clsZprava) iter.next();
                    clsZprava msg_snd = srv.sendMessage(msg);
                    if (msg_snd != null) {
                        if (msg_snd.fronta_id != -1) {
                            db.updMessage(msg_snd);
                        } else {
                            msg2uiShowPopupInfo("Zprávu '" + msg.sms + "' se nepodařilo odeslat");
                        }
                    } else {
                        snd_cnt++;
                        msg2uiShowPopupInfo("Zprávu '" + msg.sms + "' se nepodařilo odeslat");
                    }

                    if ( (getAkce() != clsTypAkce.taSEND_UNSENDED_SMS) || ((getAkce() == clsTypAkce.taSEND_UNSENDED_SMS) && getPrerusitAkci()) ) {
                        result = false;
                        return;
                    }

                }
                result = true;
            } else {
                result = true;
            }

        } catch (Exception e) {
            result = false;
        } finally {
            msg2uiSendUnsendedMessagesResult(result, snd_cnt);
        }
    }

    private void msg2uiSendUnsendedMessagesResult(Boolean res, int sended_sms_cnt) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taSEND_UNSENDED_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.arg2 = sended_sms_cnt;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiSndUnsndMgRes", e.toString());
        }
    }

    private void sendMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{
            bnd.putString("text-zpravy", zpr.sms);

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taSEND_SMS) || ((getAkce() == clsTypAkce.taSEND_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }
                clsZprava msg_ret = srv.sendMessage(zpr);

                //pokud mam ID fronty, zprava se zaradila v poradku do odesilaci fronty na serveru
                if (msg_ret != null) {
                    if (msg_ret.fronta_id != -1) {

                        //zprava se vlozila do odesilaci fronty
                        db.addMessage(msg_ret);
                        result = true;

                    }
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-sendMessageInt", e.toString());
        } finally {
            msg2uiSendMessageResult(result, bnd);
        }
    }

    private void msg2uiSendMessageResult(Boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taSEND_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiSndMsgRes", e.toString());
        }
    }

    private void msg2uiShowSendSmsProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleSendSmsDlg),
                    res.getString(R.string.descSendSmsDlg),
                    res.getString(R.string.btnCancelTextSendSmsDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShSmsSndcDlg", e.toString());
        }
    }

    private void msg2uiShowPopupInfo(String info) {
        Bundle bnd = new Bundle();
        bnd.putString("information", info);

        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taSHOW_POPUP_INFO);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiSndUnsndMgRes", e.toString());
        }
    }

    private void testNastaveniInt(clsSrvNastaveni obj) {
        String html_page = "<html><head></head><body><font size=\"2\">%s</font></body></html>";
        String html_data = "";
        String tmp_html_data = "";
        String html = "";

        try {

            //#1 - test, jestli je server OnLine
            msg2uiUpdateProgressDlg(33, "", "");
            tmp_html_data = "Testuji spojení se serverem ...<br>";
            html = String.format(html_page, html_data + tmp_html_data);
            msg2uiSettingTestResult(html);

            if (srv.IsServerOnline()) {
                html_data = html_data + "<img src=\"ok_16.png\"> server <b>" /*+ srv.getCompleteServerUrl()*/ + "</b> je spuštěn<br>";
                html = String.format(html_page, html_data);
                msg2uiSettingTestResult(html);
            } else {
                html_data = html_data + "<img src=\"error_16.png\"> server <b>" /*+ srv.getCompleteServerUrl()*/ + "</b> není spuštěn!<br>";
                html = String.format(html_page, html_data);
                msg2uiSettingTestResult(html);
                return;
            }
            if ( (getAkce() != clsTypAkce.taTEST_SETTINGS) || ((getAkce() == clsTypAkce.taTEST_SETTINGS) && getPrerusitAkci()) ) {
                return;
            }



            //#2 - test, jestli potrebuju login
            msg2uiUpdateProgressDlg(66, "", "");
            tmp_html_data = "Testuji nutnost přihlášení k serveru ...<br>";
            html = String.format(html_page, html_data + tmp_html_data);
            msg2uiSettingTestResult(html);

            int reqLogin = srv.RequireServerLogin(true);
            switch (reqLogin) {
                case -1:
                    html_data = html_data + "<img src=\"error_16.png\"> <b>nelze zjistit</b>, zda server požaduje přihlášení!<br>";
                    break;
                case 0:
                    html_data = html_data + "<img src=\"ok_16.png\"> server <b>nepožaduje přihlášení</b><br>";
                    break;
                case 1:
                    html_data = html_data + "<img src=\"info_16.png\"> server <b>požaduje přihlášení</b>!<br>";
                    break;
                default:
                    html_data = html_data + "<img src=\"error_16.png\"> <b>nelze zjistit</b>, zda server požaduje přihlášení!<br>";
            }
            html = String.format(html_page, html_data);
            msg2uiSettingTestResult(html);
            //
            if ((reqLogin != 0) && (reqLogin != 1)) {
                return;
            }
            if ( (getAkce() != clsTypAkce.taTEST_SETTINGS) || ((getAkce() == clsTypAkce.taTEST_SETTINGS) && getPrerusitAkci()) ) {
                return;
            }


            //#3 - test prihlaseni k serveru
            if (reqLogin == 1) {
                msg2uiUpdateProgressDlg(99, "", "");
                tmp_html_data = "Testuji přihlášení k serveru ...<br>";
                html = String.format(html_page, html_data + tmp_html_data);
                msg2uiSettingTestResult(html);

                if (srv.LoginToServer(true)) {
                    html_data = html_data + "<img src=\"ok_16.png\"> <b>úspěšně přihlášen</b> k serveru<br>";
                } else {
                    html_data = html_data + "<img src=\"error_16.png\"> přihlášení k serveru se <b>nezařilo</b>!<br>";
                }

                html = String.format(html_page, html_data);
                msg2uiSettingTestResult(html);
            }


        } catch (Exception e) {
            Log.d("testNastaveni", e.toString());
        }
    }

    private void msg2uiSettingTestResult(String tr) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taTEST_SETTINGS);
        msg.obj = tr;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiSetTestRes", e.toString());
        }
    }

    private void deleteQueueMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taDELETE_QUEUE_SMS) || ((getAkce() == clsTypAkce.taDELETE_QUEUE_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }

                clsZprava msg_upd = srv.deleteQueueMessage(zpr);
                if (msg_upd != null) {
                    result = db.delMessage(msg_upd);
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-delMessageInt", e.toString());
        } finally {
            msg2uiDeleteQueueMessageResult(result, bnd);
        }
    }

    private void msg2uiDeleteQueueMessageResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taDELETE_QUEUE_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiSndMsgRes", e.toString());
        }
    }

    private void pauseQueueMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taPAUSE_SMS) || ((getAkce() == clsTypAkce.taPAUSE_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }

                clsZprava msg_upd = srv.pauseQueueMessage(zpr);
                if (msg_upd != null) {
                    result = (db.updMessage(msg_upd) != -1);
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-pauQueMessageInt", e.toString());
        } finally {
            msg2uiPauseQueueMessageResult(result, bnd);
        }
    }

    private void msg2uiPauseQueueMessageResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taPAUSE_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiPauQueMsgRes", e.toString());
        }
    }

    private void resumeQueueMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taRESUME_SMS) || ((getAkce() == clsTypAkce.taRESUME_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }

                clsZprava msg_upd = srv.resumeQueueMessage(zpr);
                if (msg_upd != null) {
                    result = (db.updMessage(msg_upd) != -1);
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-resQueMessageInt", e.toString());
        } finally {
            msg2uiResumeQueueMessageResult(result, bnd);
        }
    }

    private void msg2uiResumeQueueMessageResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taRESUME_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiResQueMsgRes", e.toString());
        }
    }

    private void deleteHistoryMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taDELETE_HISTORY_SMS) || ((getAkce() == clsTypAkce.taDELETE_HISTORY_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }

                clsZprava msg_upd = srv.deleteSendedMessage(zpr);
                if (msg_upd != null) {
                    result = db.delMessage(msg_upd);
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-delHistMessageInt", e.toString());
        } finally {
            msg2uiDeleteHistoryMessageResult(result, bnd);
        }
    }

    private void msg2uiDeleteHistoryMessageResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taDELETE_HISTORY_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiDelHstMsg", e.toString());
        }
    }

    private void archiveHistoryMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taARCHIVE_SMS) || ((getAkce() == clsTypAkce.taARCHIVE_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }

                clsZprava msg_upd = srv.archiveSendedMessage(zpr);
                if (msg_upd != null) {
                    result = (db.updMessage(msg_upd) != -1);
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-archHistMessageInt", e.toString());
        } finally {
            msg2uiArchiveHistoryMessageResult(result, bnd);
        }
    }

    private void msg2uiArchiveHistoryMessageResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taARCHIVE_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiArchHstMsg", e.toString());
        }
    }

    private void unarchiveHistoryMessageInt(clsZprava zpr) {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            if (srv.ReadyForComunication()) {
                if ( (getAkce() != clsTypAkce.taUNARCHIVE_SMS) || ((getAkce() == clsTypAkce.taUNARCHIVE_SMS) && getPrerusitAkci()) ) {
                    result = false;
                    return;
                }

                clsZprava msg_upd = srv.unarchiveSendedMessage(zpr);
                if (msg_upd != null) {
                    result = (db.updMessage(msg_upd) != -1);
                }

            } else {
                bnd.putBoolean("server-not-ready", true);
            }

        } catch (Exception e) {
            Log.d("Thr-unarchHistMsgInt", e.toString());
        } finally {
            msg2uiUnarchiveHistoryMessageResult(result, bnd);
        }
    }

    private void msg2uiUnarchiveHistoryMessageResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taUNARCHIVE_SMS);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiUnaArchHstMsg", e.toString());
        }
    }

    private void checkQueueChangesInt() {
        boolean result = false;
        Bundle bnd = new Bundle();
        try{

            List<clsZprava> fronta = db.getQueueMessages();
            if (fronta.size() > 0) {
                if (srv.ReadyForComunication()) {
                    if ((getAkce() != clsTypAkce.taCHECK_QUEUE_CHANGES) || ((getAkce() == clsTypAkce.taCHECK_QUEUE_CHANGES) && getPrerusitAkci())) {
                        result = false;
                        return;
                    }

                    clsZprava msg, msg_srv = null;
                    Iterator iter = fronta.iterator();

                    while (iter.hasNext()) {
                        if ((getAkce() != clsTypAkce.taCHECK_QUEUE_CHANGES) || ((getAkce() == clsTypAkce.taCHECK_QUEUE_CHANGES) && getPrerusitAkci())) {
                            result = false;
                            return;
                        }

                        msg = (clsZprava) iter.next();
                        msg_srv = srv.checkQueueMessageState(msg);
                        if (msg_srv != null) {
                            db.updMessage(msg_srv);

                            //kdyz mam odeslanou zpravu, zobrazim info
                            if (msg_srv.odeslano_id != -1) {
                                String prijemce = msg.prezdivka.trim();
                                if (prijemce.length() > 0) {
                                    prijemce = prijemce + " (" + msg.cislo + ")";
                                } else {
                                    prijemce = msg.cislo;
                                }
                                msg2uiShowPopupInfo("Zpráva pro '" + prijemce + "' byla odeslána");
                            }

                        }
                    }

                    result = true;
                } else {
                    bnd.putBoolean("server-not-ready", true);
                }
            }

        } catch (Exception e) {
            result = false;
            Log.d("Thr-checkQueChngMsgInt", e.toString());
        } finally {
            msg2uiCheckQueueChangesResult(result, bnd);
        }
    }

    private void msg2uiCheckQueueChangesResult(boolean res, Bundle bnd) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taCHECK_QUEUE_CHANGES);
        msg.arg1 = clsFunctions.booleanToInt(res);
        msg.obj = bnd;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiCheckQueChMsg", e.toString());
        }
    }

    // -----

    private void msg2uiShowProgressDlg(String title, String message, String btnText) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taSHOW_PROGRESS_DLG);
        msg.obj = title + '|' + message + '|' + btnText;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowProgDlg", e.toString());
        }
    }

    private void msg2uiUpdateProgressDlg(int prog, String title, String desc) {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taUPDATE_PROGRESS_DLG);
        msg.arg1 = prog;
        msg.obj = title + '|' + desc;

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uiUpdateProgDlg", e.toString());
        }
    }

    private void msg2uiHhideProgressDlg() {
        Message msg = new Message();
        msg.what = clsFunctions.typ2int(clsTypAkce.taHIDE_PROGRESS_DLG);

        try {
            uiHndlr.sendMessage(msg);
        } catch (Exception e) {
            Log.d("Thr-msg2uihideProgDlg", e.toString());
        }
    }

    private boolean getPrerusitAkci() {
        synchronized (status) {
            return status.getPrerusit();
        }
    }

    private void setAkce(clsTypAkce aktualniAkce) {
        synchronized (status) {
            status.setAkce(aktualniAkce);
            status.setPrerusit(false);
        }
    }

    public void setServerSettings(clsSrvNastaveni ss) {
        synchronized (status) {
            srv.setSrv_url(ss.AdresaKrtekServeru);
            srv.setLogin(ss.Login);
            srv.setPassword(ss.Heslo);
        }
    }

    public clsTypAkce getAkce() {
        synchronized (status) {
            return status.getAkce();
        }
    }

    public void prerusAkci() {
        synchronized (status) {
            status.setPrerusit(true);
        }
    }

    private void sendThreadMsg(Message msg) {
        hndlr.sendMessage(msg);
        //hndlr.sendMessageDelayed(msg, 5000);
    }

    private void msg2uiShowSettingTestProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleTestDlg),
                    res.getString(R.string.descTestDlg),
                    res.getString(R.string.btnCancelTextTestDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowSyncDlg", e.toString());
        }
    }

    private void msg2uiShowSynchronizeProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleSynchronizeDataDlg),
                    res.getString(R.string.descSynchronizeDataDlg),
                    res.getString(R.string.btnCancelTextSynchronizeDataDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowSyncDlg", e.toString());
        }
    }

    private void msg2uiShowDeleteMessageProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleDeleteSmsDlg),
                    res.getString(R.string.descDeleteSmsDlg),
                    res.getString(R.string.btnCancelTextDeleteSmsDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowDelDlg", e.toString());
        }
    }

    private void msg2uiShowPauseQueueMessageProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titlePauseSmsDlg),
                    res.getString(R.string.descPauseSmsDlg),
                    res.getString(R.string.btnCancelTextPauseSmsDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowQuePauDlg", e.toString());
        }
    }

    private void msg2uiShowResumeQueueMessageProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleResumeSmsDlg),
                    res.getString(R.string.descResumeSmsDlg),
                    res.getString(R.string.btnCancelTextResumeSmsDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowQueResDlg", e.toString());
        }
    }

    private void msg2uiShowArchiveHistoryMessageProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleArchiveSmsDlg),
                    res.getString(R.string.descArchiveSmsDlg),
                    res.getString(R.string.btnCancelTextArchiveSmsDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowHisArDlg", e.toString());
        }
    }

    private void msg2uiShowUnarchiveHistoryMessageProgressDlg() {
        Resources res = act.getResources();
        try {
            msg2uiShowProgressDlg(
                    res.getString(R.string.titleUnarchiveSmsDlg),
                    res.getString(R.string.descUnarchiveSmsDlg),
                    res.getString(R.string.btnCancelTextUnarchiveSmsDlg)
            );
        } catch (Exception e) {
            Log.d("Thr-msg2uiShowHisUnaDlg", e.toString());
        }
    }

    public void testNastaveni(clsSrvNastaveni nast) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taTEST_SETTINGS);
            msg.obj = nast;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-testNastaveni", e.toString());
        }
    }

    public void synchronizeData(boolean wholeHistory, boolean endQuestion, boolean refreshConScriptData) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taSYNCHRONIZE_DATA);
            msg.arg1 = clsFunctions.booleanToInt(wholeHistory);
            msg.arg2 = clsFunctions.booleanToInt(endQuestion);
            msg.obj = clsFunctions.booleanToInt(refreshConScriptData);
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-synchronizeData", e.toString());
        }
    }

    public void sendUnsendetMessages() {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taSEND_UNSENDED_SMS);
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-SendUnsndMsgs", e.toString());
        }
    }

    public void sendMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taSEND_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-SendMessage", e.toString());
        }
    }

    public void deleteQueueMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taDELETE_QUEUE_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-DelQueMessage", e.toString());
        }
    }

    public void pauseQueueMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taPAUSE_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-PauQuedMessage", e.toString());
        }
    }

    public void resumeQueueMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taRESUME_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-ResQueMessage", e.toString());
        }
    }

    public void deleteHistoryMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taDELETE_HISTORY_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-DelHistMessage", e.toString());
        }
    }

    public void archiveHistoryMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taARCHIVE_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-ArchHistMessage", e.toString());
        }
    }

    public void unarchiveHistoryMessage(clsZprava zpr) {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taUNARCHIVE_SMS);
            msg.obj = zpr;
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-UnarchHistMessage", e.toString());
        }
    }

    public void checkQueueChanges() {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taCHECK_QUEUE_CHANGES);
            sendThreadMsg(msg);
        } catch (Exception e) {
            Log.d("Thr-CheckQueueMessage", e.toString());
        }
    }

}
