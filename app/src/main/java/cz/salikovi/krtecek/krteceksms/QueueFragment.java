package cz.salikovi.krtecek.krteceksms;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QueueFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QueueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QueueFragment extends Fragment implements
        AdapterView.OnItemClickListener
{
    /*
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    */

    private static int TIMER_MS_INTERVAL = 10000; //10 vterin

    private KrtecekSmsApplication myApp = null;
    private MainActivity myAct = null;
    private OnFragmentInteractionListener mListener;
    private clsFrontaAdapter daFronta = null;

    private Timer updateTimer = null;
    private clsQueueTimerTask timerTask = null;

    public static QueueFragment newInstance(KrtecekSmsApplication app, MainActivity act /*String param1, String param2*/) {
        QueueFragment fragment = new QueueFragment();
        /*
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        */
        fragment.myApp = app;
        fragment.myAct = act;
        fragment.InitAdapter();

        return fragment;
    }

    public QueueFragment() {
        // Required empty public constructor
    }

    private void InitAdapter() {
        daFronta = new clsFrontaAdapter(myAct, (ArrayList)myApp.getFronta(), myAct.getResources());
    }

    public MainActivity getMyAct() {
        return myAct;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            /*
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            */
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_queue, container, false);

        TextView _InfoLabel = (TextView) fragView.findViewById(R.id.textViewLabelFronta);
        if (_InfoLabel != null) {
            int zprav_ve_fronte = 0;

            try {
                if (myApp.getFronta() != null) {
                    zprav_ve_fronte = myApp.getFronta().size();
                }
            } catch (Exception e) {
                Log.d("QueCreateViewGetFronta", e.toString());
            }

            _InfoLabel.setText(Html.fromHtml(String.format(
                    getString(R.string.cZPRAV_K_ODESLANI), zprav_ve_fronte
            )));
        }

        ListView _FrontaListView = (ListView) fragView.findViewById(R.id.listViewFronta);
        if (_FrontaListView != null) {

            // propojim listview s adapterem
            _FrontaListView.setAdapter(daFronta);

            //nastavim listener pro tabulku
            _FrontaListView.setOnItemClickListener(this);

            //context menu
            registerForContextMenu(_FrontaListView);
        }

        updateTimer = new Timer();
        timerTask = new clsQueueTimerTask(this);
        updateTimer.scheduleAtFixedRate(timerTask, 0, TIMER_MS_INTERVAL);


        return fragView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onQueueFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        View v = getView();
        ListView lv = (ListView) v.findViewById(R.id.listViewFronta);

        int itemPos = position;
        clsZprava item = (clsZprava) lv.getItemAtPosition(position);

        //Toast.makeText(myApp, "Position :" + itemPos + "  ListItem : " + item.toString(), Toast.LENGTH_LONG).show();
        lv.showContextMenuForChild(view);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        String prijemce = "";
        switch (v.getId()) {

            case R.id.listViewFronta:
                AdapterView.AdapterContextMenuInfo infoF = (AdapterView.AdapterContextMenuInfo)menuInfo;
                clsZprava msg = (clsZprava) daFronta.getItem(infoF.position);

                //titulek menu
                prijemce = msg.prezdivka.trim();
                if (prijemce.length() > 0) {
                    prijemce = prijemce + " (" + msg.cislo + ")";
                } else {
                    prijemce = msg.cislo;
                }
                menu.setHeaderTitle(prijemce);

                //telo menu
                menu.add(R.string.cMENU_DELETE);
                if (msg.stav == clsStavZpravy.szPAUSED) {
                    menu.add(R.string.cMENU_RESUME);
                } else {
                    menu.add(R.string.cMENU_PAUSE);
                }
                break;

        } //switch
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (getUserVisibleHint()) {

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            String action_title = item.getTitle().toString();
            clsZprava msg = null;

            if (action_title.equals( getText(R.string.cMENU_DELETE) )) {
                msg = myApp.getFronta().get(info.position);
                myAct.myComThread.deleteQueueMessage(msg);

            } else if (action_title.equals( getText(R.string.cMENU_PAUSE) )) {
                msg = myApp.getFronta().get(info.position);
                myAct.myComThread.pauseQueueMessage(msg);

            } else if (action_title.equals( getText(R.string.cMENU_RESUME) )) {
                msg = myApp.getFronta().get(info.position);
                myAct.myComThread.resumeQueueMessage(msg);

            }
            return true;

        } else {
            return false;
        }
    }

    public void CheckQueueChanges() {
        myAct.myComThread.checkQueueChanges();
    }

    public boolean Refresh() {
        boolean result = false;
        try {
            View v = getView();
            List<clsZprava> fronta = myApp.getFronta();
            clsDb db = myApp.getDb();

            fronta.clear();
            List<clsZprava> msgs = db.getQueueMessages();
            if (msgs.size() > 0) {
                fronta.addAll(msgs);
            }
            daFronta.notifyDataSetChanged();

            try {
                TextView _InfoLabel = (TextView) v.findViewById(R.id.textViewLabelFronta);
                if (_InfoLabel != null) {
                    _InfoLabel.setText(Html.fromHtml(String.format(
                            getString(R.string.cZPRAV_K_ODESLANI), myApp.getFronta().size()
                    )));
                }

                ListView _FrontaList = (ListView) v.findViewById(R.id.listViewFronta);
                if (_FrontaList != null) {
                    _FrontaList.invalidateViews();
                }
            } catch (Exception e) {
                Log.d("RefreshQueue - Error", e.toString());
            }

            result = true;

        } catch (Exception e) {
            result = false;
            Log.d("RefreshQueue - Error", e.toString());
        } finally {
            return result;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Refresh();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onQueueFragmentInteraction(Uri uri);
    }

}
