package cz.salikovi.krtecek.krteceksms;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;

/**
 * Created by salik on 24. 7. 2015.
 */
public class clsHttpResponse {

    public StringBuffer data;

    public clsHttpResponse() {
        data = null;
    }

    public String getFormatedResponseData() {
        String result = "";
        if (data != null) {
            result = data.toString();
        }
        return result;
    }

    public boolean clear() {
        boolean result = false;
        try {
            if (data != null) {
                data.setLength(0);
            }
            result = true;
        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }

    }

    public boolean loadFromStream(InputStream strem, String dataCharset) {
        boolean result = false;
        String line = "";
        try {
            if (data == null) {
                data = new StringBuffer();
            }

            clear();
            BufferedReader resp_stream = new BufferedReader(new InputStreamReader( strem ));
            try {
                while ( (line = resp_stream.readLine()) != null ) {

                    if (dataCharset != "") {
                        line = URLDecoder.decode(line, dataCharset);
                    }
                    data.append(line);

                }
                result = true;
            } catch (Exception e) {
                result = false;
                Log.d("response.loadFromStream", e.toString());
            } finally {
                resp_stream.close();
            }

        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }

    }

}
