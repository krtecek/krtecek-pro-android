package cz.salikovi.krtecek.krteceksms;

/**
 * Created by salik on 7. 3. 2016.
 */
public enum clsStavZpravy {
    szUNKNOWN, szWAITING, szSENDING, szPAUSED, szEDITED, szERROR, szSENDED
}
