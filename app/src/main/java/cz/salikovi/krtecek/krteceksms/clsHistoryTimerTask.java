package cz.salikovi.krtecek.krteceksms;

import java.util.TimerTask;

/**
 * Created by salik on 7. 8. 2015.
 */
public class clsHistoryTimerTask extends TimerTask {
    private HistoryFragment frag = null;

    public clsHistoryTimerTask(HistoryFragment f) {
        frag = f;
    }

    @Override
    public void run() {
        frag.getMyAct().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                frag.Refresh();
            }

        });
    }

}

