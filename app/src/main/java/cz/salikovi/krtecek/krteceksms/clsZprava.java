package cz.salikovi.krtecek.krteceksms;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by salik on 17. 7. 2015.
 */
public class clsZprava {

    public int id;
    public int fronta_id;
    public int odeslano_id;
    public clsStavZpravy stav;
    public Date datum;
    public int kontakt_id;
    public String prezdivka;
    public String cislo;
    public String skript;
    public int skript_id;
    public String sms;
    public int archiv;

    public clsZprava() {
        //zakladni konstruktor
        id = -1;
        fronta_id = -1;
        odeslano_id = -1;
        stav = clsStavZpravy.szUNKNOWN;
        datum = null;
        kontakt_id = -1;
        prezdivka = "";
        cislo = "";
        skript = "";
        skript_id = -1;
        sms = "";
        archiv = -1;
    }

    public clsZprava(int ID, int frontaID, int odeslanoID, clsStavZpravy stav, Date datum, int kontakt_id,
                     String prezdivka, String cislo, String skript, int skript_id, String sms, int archiv)
    {
        this.id = ID;
        this.fronta_id = frontaID;
        this.odeslano_id = odeslanoID;
        this.stav = stav;
        this.datum = datum;
        this.kontakt_id = kontakt_id;
        this.prezdivka = prezdivka;
        this.cislo = cislo;
        this.skript = skript;
        this.skript_id = skript_id;
        this.sms = sms;
        this.archiv = archiv;
    }
    public clsZprava(clsZprava msg)
    {
        this.id = msg.id;
        this.fronta_id = msg.fronta_id;
        this.odeslano_id = msg.odeslano_id;
        this.stav = msg.stav;
        this.datum = msg.datum;
        this.kontakt_id = msg.kontakt_id;
        this.prezdivka = msg.prezdivka;
        this.cislo = msg.cislo;
        this.skript = msg.skript;
        this.skript_id = msg.skript_id;
        this.sms = msg.sms;
        this.archiv = msg.archiv;
    }

    @Override
    public String toString() {
        return "Zprava [id=" + id
                + ", fronta_id=" + fronta_id
                + ", stav=" + clsFunctions.NazevStavu(stav)
                + ", datum=" + getDatum("dd.MM.yyyy - HH:mm:ss")
                + ", kontakt_id=" + kontakt_id
                + ", prezdivka=" + prezdivka
                + ", cislo=" + cislo
                + ", skript=" + skript
                + ", skript_id=" + skript_id
                + ", sms=" + sms
                + ", archiv=" + archiv
                + "]";
    }

    public String getDatum(String date_format) {
        String frmtDate = "";

        if (datum != null) {
            if (date_format.equals("")) {
                date_format = "dd.MM.yyyy - HH:mm:ss";
            }
            SimpleDateFormat formatter = new SimpleDateFormat(date_format);
            frmtDate = formatter.format(datum);
        }
        return frmtDate;
    }

    public String getDatum() {
        return getDatum("");
    }

}
