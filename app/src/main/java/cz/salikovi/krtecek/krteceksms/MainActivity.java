package cz.salikovi.krtecek.krteceksms;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements
        SmsFragment.OnFragmentInteractionListener,
        QueueFragment.OnFragmentInteractionListener,
        HistoryFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener,
        Handler.Callback
{

    //pomocna promenna pro volanii synchronizace dat jen pri prvnim onResume
    private boolean firstOnResume = true;

    //indexy fragmentu
    private static final int fiSendSMS = 0;
    private static final int fiQueue = 1;
    private static final int fiHistory = 2;
    private static final int fiSettings = 3;

    private ProgressDialog pd;
    public Handler myUiHndlr;
    public KrtecekSmsApplication myApp;
    public MainActivity myAct;
    public clsComunicationThread myComThread;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //clsFunctions.saveLogcatToFile("log", "logcat.txt");

        Handler myUiHndlr = new Handler(this);

        // Create the adapter that will return a fragment for each of the four
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //jdeme na to
        myApp = (KrtecekSmsApplication) this.getApplication();
        myAct = this;

        //vytvorim pripojeni k DB
        myApp.setDb(new clsDb(this));

        //nactu nataveni programu
        myApp.NactiSrvNastaveni();

        //progress dialog
        pd = null;

        //vytvorim vlakno pro komunikaci s KrtekServerem
        myApp.setThr(new clsComunicationThread(
                    "KrtekSrvThread",
                    myApp.getDb(),
                    myApp.getSrvNastaveni(),
                    myUiHndlr,
                    this
                )
        );
        myComThread = myApp.getThr();
        myComThread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (firstOnResume) {
            //poznacim, ze uz jsem spoustel
            firstOnResume = false;

            //cekam na inicializaci vlakna
            while (myComThread.getAkce() != clsTypAkce.taLOOP) {
                SystemClock.sleep(50);
            }
            //nactu data ze serveru
            myComThread.synchronizeData(false, true, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_synchronize) {
            myComThread.synchronizeData(true, false, true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSmsFragmentInteraction(Uri uri) {
        //komunikace s fragmentem pro odeslani SMS
    }

    @Override
    public void onQueueFragmentInteraction(Uri uri) {
        //komunikace s fragmentem fronty
    }

    @Override
    public void onHistoryFragmentInteraction(Uri uri) {
        //komunikace s fragmentem historie
    }

    @Override
    public void onSettingsFragmentInteraction(Uri uri) {
        //komunikace s fragmentem nastaveni
    }

    @Override
    public boolean handleMessage(Message msg) {
        boolean result = true;
        try {
            clsTypAkce msg_type = clsFunctions.int2typ(msg.what);
            switch (msg_type) {

                case taUNDEFINED:
                    break;
                case taLOOP:
                    break;
                case taREFRESH_QUEUE:
                    refreshQueue();
                    break;
                case taREFRESH_HISTORY:
                    refreshHistory();
                    break;
                case taSYNCHRONIZE_DATA:
                    hideProgressDlg(msg); //pokud je zobrazeny, tak se skryje
                    manageDataSynchronizationResult(clsFunctions.intToBoolean(msg.arg1), clsFunctions.intToBoolean(msg.arg2));
                    break;
                case taSEND_SMS:
                    manageSendMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taPAUSE_SMS:
                    managePauseQueueMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taRESUME_SMS:
                    manageResumeQueueMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taDELETE_QUEUE_SMS:
                    manageDeleteQueueMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taDELETE_HISTORY_SMS:
                    manageDeleteHistoryMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taARCHIVE_SMS:
                    manageArchiveHistoryMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taUNARCHIVE_SMS:
                    manageUnarchiveHistoryMessageResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taSEND_UNSENDED_SMS:
                    manageUnsendedSMSResult(clsFunctions.intToBoolean(msg.arg1), msg.arg2);
                    break;
                case taCHECK_QUEUE_CHANGES:
                    manageCheckQueueChangeResult(clsFunctions.intToBoolean(msg.arg1), (Bundle) msg.obj);
                    break;
                case taSHOW_PROGRESS_DLG:
                    showProgressDlg(msg);
                    break;
                case taUPDATE_PROGRESS_DLG:
                    updateProgresDlg(msg);
                    break;
                case taHIDE_PROGRESS_DLG:
                    hideProgressDlg(msg);
                    break;
                case taTEST_SETTINGS:
                    SettingsFragment sf = getSettingsFragment();
                    if (sf != null) {
                        sf.setSettingsResult((String) msg.obj);
                    }
                    break;
                case taREFRESH_SCRIPTS:
                    readScriptData((List<clsSkript>) msg.obj);
                    break;
                case taREFRESH_CONTACTS:
                    readContactData((List<clsKontakt>) msg.obj);
                    break;
                case taREFRESH_MESSAGES:
                    reloadMessagesData();
                    break;
                case taSHOW_POPUP_INFO:
                    showPopupInfo((Bundle) msg.obj);
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            result = false;
            Log.d("UiThr-HndlMsg", e.toString());
        } finally {
            return result;
        }
    }

    private void readContactData(List<clsKontakt> con) {
        myApp.getKontakty().clear();
        try {
            myApp.getKontakty().addAll(con);
            refreshSmsFrag();
        } catch (Exception e) {
            Log.d("readContactData", e.toString());
        }
    }

    private void reloadMessagesData() {
        refreshQueue();
        refreshHistory();
    }

    private void readScriptData(List<clsSkript> scr) {
        myApp.getSkripty().clear();
        try {
            myApp.getSkripty().addAll(scr);
            refreshSmsFrag();
        } catch (Exception e) {
            Log.d("readScriptData", e.toString());
        }
    }

    public void manageDataSynchronizationResult(boolean syncResult, boolean endQuestion) {
        if (!syncResult && endQuestion) {
            CloseAppConfirmationDialog(
                getString(R.string.cUKONCENI_PROGRAMU_NADPIS),
                getString(R.string.cUKONCENI_PROGRAMU_TEXT)
            );
        }

        refreshQueue();
        refreshHistory();

        if (!endQuestion) {
            Context c = myAct.getApplicationContext();
            try {
                if (syncResult) {
                    //odeslu zpravy z fronty, ktere nemaji "fronta_id"
                    myComThread.sendUnsendetMessages();

                    Toast.makeText(c, R.string.cSYNC_OK, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(c, R.string.cSYNC_FAIL, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d("manageDataSyncResult", e.toString());
            }
        }
    }

    public void showPopupInfo(Bundle bnd) {
        try {
            Toast.makeText(getApplicationContext(), bnd.getString("information"), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.d("showPopupInfo", e.toString());
        }
    }

    public void manageUnsendedSMSResult(boolean res, int msg_cnt) {
        if (res) {
            if (msg_cnt > 0) {
                Toast.makeText(getApplicationContext(), R.string.cSND_UNSENDED_OK, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.cSND_UNSENDED_FAIL, Toast.LENGTH_LONG).show();
        }
    }

    public void manageCheckQueueChangeResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshQueue();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        //nezobrazuju info, protoze kdyz se ztrati pripojeni, tak by me to porad hlasilo, ze neni spojeni se serverem
                        //Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    }
                }
            }
        } catch (Exception e) {
            Log.d("manageChckQueChngResult", e.toString());
        }
    }

    public void manageSendMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshQueue();
                Toast.makeText(getApplicationContext(), R.string.cSEND_ZPRAVA_ZARAZENA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("text-zpravy")) {
                    setMsgTxtSmsFrag(bnd.getString("text-zpravy"));
                }
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cSEND_ZPRAVA_NEZARAZENA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cSEND_ZPRAVA_NEZARAZENA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageSendMessageResult", e.toString());
        }
    }

    public void manageDeleteQueueMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshQueue();
                Toast.makeText(getApplicationContext(), R.string.cZPRAVA_SMAZANA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NESMAZANA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NESMAZANA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageDelQueMsgResult", e.toString());
        }
    }

    public void manageDeleteHistoryMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshHistory();
                Toast.makeText(getApplicationContext(), R.string.cZPRAVA_SMAZANA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NESMAZANA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NESMAZANA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageDelHistMsgResult", e.toString());
        }
    }

    public void manageArchiveHistoryMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshHistory();
                Toast.makeText(getApplicationContext(), R.string.cZPRAVA_ARCHIVOVANA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEARCHIVOVANA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEARCHIVOVANA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageArchHistMsgResult", e.toString());
        }
    }

    public void manageUnarchiveHistoryMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshHistory();
                Toast.makeText(getApplicationContext(), R.string.cZPRAVA_ODARCHIVOVANA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEODARCHIVOVANA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEODARCHIVOVANA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageArchHistMsgResult", e.toString());
        }
    }

    public void manageResumeQueueMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshQueue();
                Toast.makeText(getApplicationContext(), R.string.cZPRAVA_OBNOVENA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEOBNOVENA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEOBNOVENA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageDelQueMsgResult", e.toString());
        }
    }

    public void managePauseQueueMessageResult(boolean res, Bundle bnd) {
        try {
            if (res) {
                refreshQueue();
                Toast.makeText(getApplicationContext(), R.string.cZPRAVA_POZASTAVENA, Toast.LENGTH_LONG).show();
            } else {
                if (bnd.containsKey("server-not-ready")) {
                    if (bnd.getBoolean("server-not-ready")) {
                        Toast.makeText(getApplicationContext(), R.string.cSERVER_OFFLINE, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEPOZASTAVENA, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.cZPRAVA_NEPOZASTAVENA, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.d("manageDelQueMsgResult", e.toString());
        }
    }

    public void showProgressDlg(Message msg) {
        String str = (String) msg.obj;
        String[] data = str.split("\\|");
        pd = clsFunctions.createCancelProgressDialog(this, pd, data[0], data[1], data[2], myComThread);
        pd.show();
    }

    public void updateProgresDlg(Message msg) {
        if (pd != null) {
            int prog = msg.arg1;
            String str = (String) msg.obj;
            String[] data = str.split("\\|");

            pd.setProgress(prog);

            if (data.length > 1) {
                pd.setMessage(data[1]);
                if (data[0] != "") {
                    pd.setTitle(data[0]);
                }
            } else {
                if (data.length == 1) {
                    if (data[0] != "") {
                        pd.setTitle(data[0]);
                    }
                }
            }
        }
    }

    public void hideProgressDlg(Message msg) {
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case fiSendSMS:
                    return SmsFragment.newInstance(myApp, myAct);
                case fiQueue:
                    return QueueFragment.newInstance(myApp, myAct);
                case fiHistory:
                    return HistoryFragment.newInstance(myApp, myAct);
                case fiSettings:
                    return SettingsFragment.newInstance(myApp, myAct);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case fiSendSMS:
                    return getString(R.string.titleFragSend).toUpperCase(l);
                case fiQueue:
                    return getString(R.string.titleFragQueue).toUpperCase(l);
                case fiHistory:
                    return getString(R.string.titleFragHist).toUpperCase(l);
                case fiSettings:
                    return getString(R.string.titleFragSett).toUpperCase(l);
            }
            return null;
        }
    }

    public QueueFragment getQueueFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Object f = null;
        QueueFragment result = null;

        Iterator iter = fm.getFragments().iterator();
        while (iter.hasNext()) {
            f = iter.next();
            if (f instanceof QueueFragment) {
                result = (QueueFragment)f;
            }
        }
        return result;
    }

    public HistoryFragment getHistoryFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Object f = null;
        HistoryFragment result = null;

        Iterator iter = fm.getFragments().iterator();
        while (iter.hasNext()) {
            f = iter.next();
            if (f instanceof HistoryFragment) {
                result = (HistoryFragment)f;
            }
        }
        return result;
    }

    public SmsFragment getSmsFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Object f = null;
        SmsFragment result = null;

        Iterator iter = fm.getFragments().iterator();
        while (iter.hasNext()) {
            f = iter.next();
            if (f instanceof SmsFragment) {
                result = (SmsFragment)f;
            }
        }
        return result;
    }

    public SettingsFragment getSettingsFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Object f = null;
        SettingsFragment result = null;

        Iterator iter = fm.getFragments().iterator();
        while (iter.hasNext()) {
            f = iter.next();
            if (f instanceof SettingsFragment) {
                result = (SettingsFragment)f;
            }
        }
        return result;
    }

    public void refreshSmsFrag() {
        SmsFragment f;

        f = getSmsFragment();
        if (f != null) {
            f.Refresh();
        }
    };

    public void setMsgTxtSmsFrag(String smsTxt) {
        SmsFragment f;

        f = getSmsFragment();
        if (f != null) {
            f.setSmsText(smsTxt);
        }
    }

    public void msgRefreshQueue() {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taREFRESH_QUEUE);
            myUiHndlr.sendMessageDelayed(msg, 2000);
        } catch (Exception e) {
            Log.d("msgRefreshQueue", e.toString());
        }
    }

    public void refreshQueue() {
        QueueFragment f;

        f = getQueueFragment();
        if (f != null) {
            f.Refresh();
        } else {
            msgRefreshQueue();
            Log.d("refreshQueue", "queueFragment does not exists!");
        }
    };

    public void msgRefreshHistory() {
        Message msg = new Message();
        try {
            msg.what = clsFunctions.typ2int(clsTypAkce.taREFRESH_HISTORY);
            myUiHndlr.sendMessageDelayed(msg, 2000);
        } catch (Exception e) {
            Log.d("msgRefreshQueue", e.toString());
        }
    }

    public void refreshHistory() {
        HistoryFragment f;

        f = getHistoryFragment();
        if (f != null) {
            f.Refresh();
        } else {
            msgRefreshHistory();
            Log.d("refreshHistory", "historyFragment does not exists!");
        }
    };

    private static String makeFragmentName(int viewPagerId, int index) {
        return "android:switcher:" + viewPagerId + ":" + index;
    }

    public void CloseAppConfirmationDialog(String title, String text) {
        try {
            AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(this);
            dlgBuilder.setTitle(title);
            dlgBuilder.setMessage(text);
            dlgBuilder.setCancelable(true);
            dlgBuilder.setPositiveButton("Ano",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //finish();
                        }
                    });
            dlgBuilder.setNegativeButton("Ne",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish(); //close app
                        }
                    });

            AlertDialog dlgMsg = dlgBuilder.create();
            dlgMsg.setCancelable(false); //modal
            dlgMsg.show();

        } catch (Exception e) {
            Log.d("CloseConfirmationDialog", e.toString());
        }
    }

    @Override
    public void onDestroy() {
        //rusim vlakno
        if (myComThread != null) {
            myComThread.interrupt();
        }

        super.onDestroy();
    }
}
