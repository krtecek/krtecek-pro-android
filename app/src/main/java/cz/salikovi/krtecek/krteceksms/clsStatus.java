package cz.salikovi.krtecek.krteceksms;

/**
 * Created by salik on 7. 3. 2016.
 */
public class clsStatus {
    private clsTypAkce akce;
    private boolean prerusit;

    clsStatus() {
        akce = clsTypAkce.taUNDEFINED;
        prerusit = false;
    }

    public clsTypAkce getAkce() {
        return akce;
    }

    public void setAkce(clsTypAkce newAkce) {
        akce = newAkce;
    }

    public boolean getPrerusit() {
        return prerusit;
    }

    public void setPrerusit(boolean newPrerusit) {
        prerusit = newPrerusit;
    }
}
