package cz.salikovi.krtecek.krteceksms;

import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by salik on 13. 3. 2016.
 */
public class clsServerUrlTextChangeWatcher implements TextWatcher {
    private String regexpPattern;
    private EditText edtText;

    public clsServerUrlTextChangeWatcher(EditText et, String pattern) {
        regexpPattern = pattern;
        edtText = et;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String value  = s.toString();

        if(value.matches(regexpPattern)) {
            edtText.setTextColor(Color.BLACK);
        } else {
            edtText.setTextColor(Color.RED);
        }
    }
}
