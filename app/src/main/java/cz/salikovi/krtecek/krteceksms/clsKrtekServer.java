package cz.salikovi.krtecek.krteceksms;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by salik on 22. 7. 2015.
 */
public class clsKrtekServer {

    private static final int HTTP_TIMEOUT = 10000;  //10s

    private static final String RESP_WAIT_FOR_LOGIN = "waiting for login";
    private static final String RESP_SRV_PAUSED = "server is paused";
    private static final String RESP_WAIT_FOR_SMS = "waiting for sms";
    private static final String RESP_OK = "ok";
    private static final String RESP_ERROR = "error";

    private static final String CRLF = System.getProperty("line.separator");
    private static final String PARAM_ENCODE = "UTF-8";
    private static final String USER_AGENT = "SaLik Http Client 0.1";

    private clsHttpCookies cookies;
    private clsHttpHeader header;
    private clsHttpResponse response;

    private String srv_url;
    private String login;
    private String password;

    private boolean ready;

    public clsKrtekServer() {
        this("https://localhost:8080", "", "");
    }

    public clsKrtekServer(String SrvUrl, String SrvLogin, String SrvPass) {
        srv_url = SrvUrl;
        login = SrvLogin;
        password = SrvPass;

        cookies = new clsHttpCookies();
        header = new clsHttpHeader();
        response = new clsHttpResponse();

        ready = false;
    }

    public boolean getReady() {
        return ready;
    }

    public String getLogin () {
        return login;
    }

    public void setLogin(String login) {
       this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSrv_url() {
        return srv_url;
    }

    public void setSrv_url (String url) {
        srv_url = url;
    }

    public String getCookies() {
        return cookies.getFormatedCookies();
    }

    public String getHeader() {
        return header.getFormatedHeaderData();
    }

    public String getResponse() {
        return response.getFormatedResponseData();
    }

    private String buildParamString(ContentValues params) {
        String result = "";

        if  (params != null) {
            if (params.size() > 0) {
                Set<Map.Entry<String, Object>> param_set = params.valueSet();
                Iterator iter = param_set.iterator();

                while (iter.hasNext()) {
                    Map.Entry me = (Map.Entry) iter.next();
                    if (result != "") {
                        result = result + "&";
                    }
                    try {
                        result = result + URLEncoder.encode(me.getKey().toString(), PARAM_ENCODE) + "=" + URLEncoder.encode(me.getValue().toString(), PARAM_ENCODE);
                    } catch (Exception e) {
                        Log.d("URLEncoder.encode - err", me.toString() + ", " + PARAM_ENCODE);
                    }
                }

            }
        }

        return result;
    }

    private boolean checkResultCode(int resultCode, ContentValues acceptedResultCodes) {
        boolean result = false;
        try {

            if  (acceptedResultCodes != null) {
                if (acceptedResultCodes.size() > 0) {
                    Set<Map.Entry<String, Object>> result_set = acceptedResultCodes.valueSet();
                    Iterator iter = result_set.iterator();

                    while (iter.hasNext()) {
                        Map.Entry me = (Map.Entry) iter.next();
                        if ( Integer.parseInt(me.getValue().toString()) == resultCode ) {
                            result = true;
                            break;
                        }
                    }

                } else {
                    result = true;
                }
            } else {
                result = true;
            }

        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }
    }

    private boolean isSSL(String url) {
        return url.toLowerCase().contains("https://");
    }

    public boolean HttpPostRequest(String url, ContentValues parameters, ContentValues result_codes) {
        boolean result = false;
        int result_code = -1;
        String ReqUrl = "";
        String par = "";
        byte[] parBuffer;

        //adresa skriptu
        if (isSSL(this.srv_url)) {
            ReqUrl = this.srv_url + "/" + url;
        } else {
            ReqUrl = this.srv_url + "/" + url;
        }

        //pridam parametr pro RESTovou odpoved
        if (parameters == null) {
            parameters =  new ContentValues();
        }
        parameters.put("rest", "1");

        //sestavim parametry
        par = buildParamString(parameters);
        try {
            parBuffer = par.getBytes(PARAM_ENCODE);
        } catch (Exception e) {
            parBuffer = new byte[0];
        }

        //provedu pozadavek
        HttpURLConnection con = null;
        try {
            URL socket = new URL(ReqUrl);
            if (isSSL(this.srv_url)) {
                SSLContext SSLc = SSLContext.getInstance("TLS");
                SSLc.init(null, new TrustManager[] { new clsAcceptAllCertificatesTrustManager() }, new SecureRandom());
                con = (HttpsURLConnection) socket.openConnection();
                ((HttpsURLConnection) con).setSSLSocketFactory(SSLc.getSocketFactory());
                ((HttpsURLConnection) con).setHostnameVerifier(new clsTrustAllHostNameVerifier());
            } else {
                con = (HttpURLConnection) socket.openConnection();
            }

            con.setUseCaches(false);
            con.setConnectTimeout(HTTP_TIMEOUT);
            con.setReadTimeout(HTTP_TIMEOUT);

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Cookie", getCookies());
            con.setRequestProperty("Accept-Charset", PARAM_ENCODE);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=" + PARAM_ENCODE);
            con.setRequestProperty("Content-Length", Integer.toString(parBuffer.length));
            con.setUseCaches(false);

            con.setInstanceFollowRedirects(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            //odeslu POST data
            DataOutputStream out = new DataOutputStream( con.getOutputStream() );
            out.write(parBuffer);

            result_code = con.getResponseCode();

            header.loadFromConnection(con);
            cookies.loadFromHeader(header);
            response.loadFromStream(con.getInputStream(), header.getCharset());

            //zkontroluju result code a podle toho nastavim, zda se nacteni povedlo
            result = checkResultCode(result_code, result_codes);

        } catch (Exception e) {
            result = false;
            Log.d("HttpPostRequest", e.toString());
        } finally {
            if (con != null) {
                con.disconnect();
                con = null;
            }
        }

        return result;
    }

    public boolean HttpGetRequest(String url, ContentValues parameters, ContentValues result_codes) {
        boolean result = false;
        int result_code = -1;
        String ReqUrl = "";
        String par = "";

        //adresa skriptu
        if (isSSL(this.srv_url)) {
            ReqUrl =  this.srv_url + "/" + url;
        } else {
            ReqUrl =  this.srv_url + "/" + url;
        }

        //sestavim parametry
        if (parameters == null) {
            parameters =  new ContentValues();
        }
        parameters.put("rest", "1");    //pridam REST
        par = buildParamString(parameters);
        if (ReqUrl.contains("?")) {
            ReqUrl = ReqUrl + "&" + par;
        } else {
            ReqUrl = ReqUrl + "?" + par;
        }

        //provedu pozadavek
        HttpURLConnection con = null;
        try {
            URL socket = new URL(ReqUrl);
            if (isSSL(this.srv_url)) {
                SSLContext SSLc = SSLContext.getInstance("TLS");
                SSLc.init(null, new TrustManager[] { new clsAcceptAllCertificatesTrustManager() }, new SecureRandom());
                con = (HttpsURLConnection) socket.openConnection();
                ((HttpsURLConnection) con).setSSLSocketFactory(SSLc.getSocketFactory());
                ((HttpsURLConnection) con).setHostnameVerifier(new clsTrustAllHostNameVerifier());
            } else {
                con = (HttpURLConnection) socket.openConnection();
            }

            con.setUseCaches(false);
            con.setConnectTimeout(HTTP_TIMEOUT);
            con.setReadTimeout(HTTP_TIMEOUT);

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Cookie", getCookies());
            con.setRequestProperty("Accept-Charset", PARAM_ENCODE);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=" + PARAM_ENCODE);

            result_code = con.getResponseCode();

            header.loadFromConnection(con);
            cookies.loadFromHeader(header);
            response.loadFromStream(con.getInputStream(), header.getCharset());

            //zkontroluju result code a podle toho nastavim, zda se nacteni povedlo
            result = checkResultCode(result_code, result_codes);

        } catch (Exception e) {
            result = false;
            Log.d("HttpGetRequest", e.toString());
        } finally {
            if (con != null) {
                con.disconnect();
                con = null;
            }
        }

        return result;
    }

    public boolean IsServerOnline() {
        ContentValues result_codes = new ContentValues();
        result_codes.put(null, "200");

        return HttpGetRequest("", null, result_codes);
    }

    public int RequireServerLogin(boolean UseLastResponse) {
        int result = -1; //nepodarilo se zjistit
        String val;
        JSONObject jObj;

        try {
            //nactu "hlavni" stranku serveru, pokud nechci pouzit posledni stranku
            if (!UseLastResponse) {
                IsServerOnline();
            }

            try {

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {

                    val = jObj.getString("response");
                    if (val.equals(RESP_WAIT_FOR_LOGIN)) {
                        result = 1; //je potreba login
                    } else {
                        if (jObj.has("logout-link")) {
                            result = 1;
                        } else {
                            result = 0; //neni potreba login
                        }
                    }

                } else {
                    result = -1;
                }

            } catch (Exception e) {
                result = -1;
                Log.d("RequireServerLogin", e.toString());
            }

        } catch (Exception e) {
            result = -1;
            Log.d("RequireServerLogin", e.toString());
        } finally {
            return result;
        }

    }

    public int RequireServerLogin () {
        return RequireServerLogin(false);
    }

    public boolean LoginToServer(boolean UseLastResponse) {
        boolean result = false;
        boolean preTest = false;
        JSONObject jObj;
        String val;
        String resp;

        try {
            if (UseLastResponse) {
                preTest = UseLastResponse;
            } else {
                preTest = IsServerOnline();
            }

            //pokud uz mam neco nacteno, zjistuju, co mi to ukazuje, at se zbytecne neprihlasuju znova
            if (preTest) {
                try {
                    jObj = new JSONObject(getResponse());
                    if (jObj.length() != 0) {
                        val = jObj.getString("response");
                        //kdyz mam predchozi odpoved cokoliv jineho nez "cekam na login", jsem prihlaseny
                        result = ( !val.equals(RESP_WAIT_FOR_LOGIN) );
                    }
                } catch (Exception e) {
                    result = false;
                }
            } //end pretest

            if (!result) {
                //pokud se zjsitilo, ze jsem prihlaseny, tak uz se prihlasovat nemusim

                ContentValues result_codes = new ContentValues();
                result_codes.put(null, "200");

                ContentValues parameters = new ContentValues();
                parameters.put("akce", "login");
                parameters.put("usr", login);
                parameters.put("psw", password);

                if (HttpPostRequest("", parameters, result_codes)) {
                    //zkontroluju odezvu serveru

                    jObj = new JSONObject(getResponse());
                    if (jObj.length() != 0) {
                        val = jObj.getString("response");
                        //kdyz cekam na sms nebo je server pozastaven, prihlaseni se podarilo
                        result = ((val.equals(RESP_WAIT_FOR_SMS)) || (val.equals(RESP_SRV_PAUSED)));
                    }
                }
            }

        } catch (Exception e) {
            Log.d("LoginToServer", e.toString());
            result = false;
        } finally {
            return result;
        }
    }

    public boolean LoginToServer() {
        return LoginToServer(false);
    }

    public boolean ReadyForComunication() {
        boolean result = false;
        boolean useLastResponse = true;
        try {

            if ( IsServerOnline() ) {
                int login_result = RequireServerLogin(useLastResponse);
                switch (login_result) {
                    case -1: //chyba pri zjistovani
                        result = false;
                        break;
                    case 0: //nepotrebuju login
                        result = true;
                        break;
                    case 1: //potrebuju login
                        result = LoginToServer(useLastResponse);
                        break;
                    default:
                        result = false;
                } //switch
            } //is online

        } catch (Exception e) {
            Log.d("ReadyForComunication", e.toString());
            result = false;
        } finally {
            ready = result;
            return result;
        }
    }

    public int ReadContactsFromServer(int contactsCount, List<clsKontakt> kontakty, String lastChange) {
        int result = 0;
        JSONObject jObj;
        Bitmap bmp = null;
        byte[] decodedString;

        try {
            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "kontakty");
            parameters.put("last-change", lastChange);

            if (HttpGetRequest("", parameters, result_codes)) {
                //zkontroluju odezvu serveru

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {

                    //pokud je na serveru min zaznamu nez v telefonu (neco se smazalo), nactu vsechna data
                    if (jObj.getInt("all-row-cnt") < contactsCount) {
                        ReadContactsFromServer(0, kontakty, "");
                        result = -1; //vymazat vsechny kontakty a nacist znovu
                    } else {
                        if (jObj.has("contacts")) {
                            JSONArray jArr = jObj.getJSONArray("contacts");
                            for (int i = 0; i < jArr.length(); i++) {
                                JSONObject arrObj = jArr.getJSONObject(i);

                                //nactu avatar
                                bmp = clsFunctions.base64ToBitmap(arrObj.getString("avatar"));

                                //pridavam do kontaktu
                                kontakty.add(new clsKontakt(
                                        arrObj.getInt("id"),
                                        arrObj.getString("nickname"),
                                        arrObj.getString("number"),
                                        arrObj.getInt("srv-script-id"),
                                        bmp,
                                        arrObj.getString("upd_ts")
                                ));

                                //arrObj.getBoolean("selected")     //jestli je v combu vybrany
                                //arrObj.getString("script-name")   //jmeno skriptu, pres ktery se danemu kontaktu posila sms
                            }
                        }
                        result = 1; //vse ok
                    }

                }
            } else {
                Log.d("ReadContactsFromServer", "Nepodařilo se načíst úvodní stránku serveru!");
                result = 0; //chyba
            }

        } catch (Exception e) {
            Log.d("ReadContactsFromServer", e.toString());
            result = 0; //chyba
        } finally {
            return  result;
        }
    }

    public int ReadScriptsFromServer(int scriptsCount, List<clsSkript> skripty, String lastChange) {
        int result = 0;
        JSONObject jObj;
        Bitmap bmp = null;

        try {
            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "skripty");
            parameters.put("last-change", lastChange);

            if (HttpGetRequest("", parameters, result_codes)) {
                //zkontroluju odezvu serveru

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {

                    //pokud je na serveru min zaznamu nez v telefonu (neco se smazalo), nactu vsechna data
                    if (jObj.getInt("all-row-cnt") < scriptsCount) {
                        ReadScriptsFromServer(0, skripty, "");
                        result = -1;  //smazat tabulku skriptu
                    } else {
                        if (jObj.has("scripts")) {
                            JSONArray jArr = jObj.getJSONArray("scripts");
                            for (int i = 0; i < jArr.length(); i++) {
                                JSONObject arrObj = jArr.getJSONObject(i);

                                //prevedu obrazek
                                bmp = clsFunctions.base64ToBitmap(arrObj.getString("image"));

                                //pridavam do kontaktu
                                skripty.add(new clsSkript(
                                        arrObj.getInt("id"),
                                        arrObj.getString("name"),
                                        bmp,
                                        arrObj.getString("upd_ts")
                                ));

                                //arrObj.getBoolean("selected")     //jestli je v combu vybrany
                            }
                        }
                        result = 1; //ok
                    }
                }

            } else {
                Log.d("ReadScriptsFromServer", "Nepodařilo se načíst úvodní stránku serveru!");
                result = 0; //chyba
            }

        } catch (Exception e) {
            Log.d("ReadScriptsFromServer", e.toString());
            result = 0; //chyba
        } finally {
            return  result;
        }
    }

    public Boolean ReadQueueFromServer(List<clsZprava> zpravy, int lastFrontaID) {
        boolean result = false;
        JSONObject jObj;

        try {
            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "fronta");
            parameters.put("last-id", lastFrontaID);

            if (HttpGetRequest("", parameters, result_codes)) {
                //zkontroluju odezvu serveru

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {
                    if (jObj.has("data-array")) {

                        JSONArray jArr = jObj.getJSONArray("data-array");
                        for (int i=0; i < jArr.length(); i++) {
                            JSONObject arrObj = jArr.getJSONObject(i);

                            //pridavam do seznamu
                            zpravy.add(new clsZprava(
                                    -1, //id v lokalni DB (to teprve doplnim)
                                    arrObj.getInt("id"),  //fronta_id
                                    -1, //id odeslane zpravy (jeste neni, protoze zprava neni odeslana)
                                    clsFunctions.int2stav(arrObj.getInt("state")),
                                    clsFunctions.str2date(arrObj.getString("date")),
                                    arrObj.getInt("contact-id"),
                                    arrObj.getString("nickname"),
                                    arrObj.getString("recipient-number"),
                                    arrObj.getString("script-name"),
                                    arrObj.getInt("script-id"),
                                    arrObj.getString("msg-text"),
                                    -1
                            ));

                            /*
                            arrObj.getInt("parts-cnt");
                            arrObj.getInt("parts-sended");
                            arrObj.getString("delete-link");
                            arrObj.getString("continue-link");
                            arrObj.getString("pause-link");
                            */

                        }

                    }
                }

                result = true;
            } else {
                Log.d("ReadQueueFromServer", "Nepodařilo se načíst frontu zpráv serveru!");
                result = false;
            }

        } catch (Exception e) {
            Log.d("ReadQueueFromServer", e.toString());
            result = false;
        } finally {
            return  result;
        }
    }

    public Boolean ReadSendedFromServer(List<clsZprava> zpravy, int lastFrontaID) {
        boolean result = false;
        JSONObject jObj;

        try {
            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "odeslano");
            parameters.put("last-id", lastFrontaID);

            if (HttpGetRequest("", parameters, result_codes)) {
                //zkontroluju odezvu serveru

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {
                    if (jObj.has("data-array")) {

                        JSONArray jArr = jObj.getJSONArray("data-array");
                        for (int i=0; i < jArr.length(); i++) {
                            JSONObject arrObj = jArr.getJSONObject(i);

                            //pridavam do seznamu
                            zpravy.add(new clsZprava(
                                    -1, //id v lokalni DB (to teprve doplnim)
                                    arrObj.getInt("queue-id"),  //fronta_id
                                    arrObj.getInt("id"), //id odeslane zpravy
                                    clsStavZpravy.szSENDED, //odeslano
                                    clsFunctions.str2date(arrObj.getString("date")),
                                    arrObj.getInt("contact-id"),
                                    arrObj.getString("nickname"),
                                    arrObj.getString("recipient-number"),
                                    arrObj.getString("script-name"),
                                    arrObj.getInt("script-id"),
                                    arrObj.getString("msg-text"),
                                    arrObj.getInt("archived")
                            ));

                            /*
                            arrObj.getString("delete-link");
                            arrObj.getString("archive-link");
                            arrObj.getString("unarchive-link");
                             */

                        }

                    }
                }

                result = true;
            } else {
                Log.d("ReadSendedFromServer", "Nepodařilo se načíst seznam odeslaných zpráv serveru!");
                result = false;
            }

        } catch (Exception e) {
            Log.d("ReadSendedFromServer", e.toString());
            result = false;
        } finally {
            return  result;
        }
    }

    public clsZprava sendMessage(clsZprava msg) {
        clsZprava result = null;
        JSONObject jObj;
        try {

            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "send-sms");
            parameters.put("sms-text", msg.sms);
            if (msg.cislo != "") {
                parameters.put("number", msg.cislo);
            }
            if (msg.skript_id != -1) {
                parameters.put("scr-db-id", Integer.toString(msg.skript_id));
            }
            if (msg.kontakt_id != -1) {
                parameters.put("con-db-id", Integer.toString(msg.kontakt_id));
            }

            if (HttpPostRequest("", parameters, result_codes)) {

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {
                    if (jObj.has("response")) {
                        String resp = jObj.getString("response");
                        if (resp.equals(RESP_OK)) {

                            //odezva je v poradku, kdyz bylo vsechno OK, tak bych jeste mel mit data o zprave
                            if (jObj.has("data-array")) {
                                JSONArray jArr = jObj.getJSONArray("data-array");
                                JSONObject arrObj = jArr.getJSONObject(0);  //mela by tam byt jen jedna polozka

                                msg.fronta_id = arrObj.getInt("id");
                                msg.stav = clsFunctions.int2stav(arrObj.getInt("state"));
                                msg.cislo = arrObj.getString("recipient-number");
                                msg.prezdivka = arrObj.getString("nickname");
                                msg.skript = arrObj.getString("script-name");
                                msg.sms = arrObj.getString("msg-text");
                                msg.datum = clsFunctions.str2date(arrObj.getString("date")/*, "dd.MM.yyyy HH:mm:ss"*/);

                                //vratim zpravu
                                result = msg;


                                // arrObj.getInt("parts-cnt");
                                // arrObj.getInt("parts-sended");
                                // arrObj.getString("delete-link");
                                // arrObj.getString("continue-link");
                                // arrObj.getString("pause-link");

                            }

                        }


                    }
                }

            }

        } catch (Exception e) {
            Log.d("sendMessage - error", e.toString());
            result = null;
        } finally {
            return result;
        }
    }

    public clsZprava modifyQueueMessage(clsZprava msg, String ukol) {
        clsZprava result = null;
        JSONObject jObj;
        try {

            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "fronta");
            parameters.put("ukol", ukol);
            parameters.put("id", msg.fronta_id);

            if (HttpPostRequest("", parameters, result_codes)) {

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {
                    if (jObj.has("response")) {
                        String resp = jObj.getString("response");
                        if (resp.equals(RESP_OK)) {

                            if (!ukol.equals("smaz")) {
                                //odezva je v poradku, kdyz bylo vsechno OK, tak bych jeste mel mit data o zprave
                                if (jObj.has("data-array")) {
                                    JSONArray jArr = jObj.getJSONArray("data-array");
                                    JSONObject arrObj = jArr.getJSONObject(0);  //mela by tam byt jen jedna polozka

                                    msg.fronta_id = arrObj.getInt("id");
                                    msg.stav = clsFunctions.int2stav(arrObj.getInt("state"));
                                    msg.cislo = arrObj.getString("recipient-number");
                                    msg.prezdivka = arrObj.getString("nickname");
                                    msg.skript = arrObj.getString("script-name");
                                    msg.sms = arrObj.getString("msg-text");
                                    msg.datum = clsFunctions.str2date(arrObj.getString("date")/*, "dd.MM.yyyy HH:mm:ss"*/);

                                    //vratim zpravu
                                    result = msg;

                                    /*
                                    arrObj.getInt("parts-cnt");
                                    arrObj.getInt("parts-sended");
                                    arrObj.getString("delete-link");
                                    arrObj.getString("continue-link");
                                    arrObj.getString("pause-link");
                                    */

                                }
                            } else {
                                //pri smazani nemam info o zprave, tak vracim vstupni zpravu
                                result = msg;
                            }

                        }


                    }
                }

            }

        } catch (Exception e) {
            Log.d("modifyQueueMessage-err", e.toString());
            result = null;
        } finally {
            return result;
        }
    }

    public clsZprava pauseQueueMessage(clsZprava msg) {
        return modifyQueueMessage(msg, "pozastav");
    }

    public clsZprava resumeQueueMessage(clsZprava msg) {
        return modifyQueueMessage(msg, "obnov");
    }

    public clsZprava deleteQueueMessage(clsZprava msg) {
        return modifyQueueMessage(msg, "smaz");
    }

    public clsZprava modifySendedMessage(clsZprava msg, String ukol) {
        clsZprava result = null;
        JSONObject jObj;
        try {

            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "odeslano");
            parameters.put("ukol", ukol);
            parameters.put("id", msg.odeslano_id);

            if (HttpPostRequest("", parameters, result_codes)) {

                jObj = new JSONObject(getResponse());
                if (jObj.length() != 0) {
                    if (jObj.has("response")) {
                        String resp = jObj.getString("response");
                        if (resp.equals(RESP_OK)) {

                            if (!ukol.equals("smaz")) {
                                //odezva je v poradku, kdyz bylo vsechno OK, tak bych jeste mel mit data o zprave
                                if (jObj.has("data-array")) {
                                    JSONArray jArr = jObj.getJSONArray("data-array");
                                    JSONObject arrObj = jArr.getJSONObject(0);  //mela by tam byt jen jedna polozka

                                    msg.fronta_id = arrObj.getInt("id");
                                    msg.cislo = arrObj.getString("recipient-number");
                                    msg.prezdivka = arrObj.getString("nickname");
                                    msg.skript = arrObj.getString("script-name");
                                    msg.sms = arrObj.getString("msg-text");
                                    msg.datum = clsFunctions.str2date(arrObj.getString("date")/*, "dd.MM.yyyy HH:mm:ss"*/);
                                    msg.archiv = arrObj.getInt("archived");

                                    //vratim zpravu
                                    result = msg;

                                    // arrObj.getInt("parts-cnt");
                                    // arrObj.getInt("parts-sended");
                                    // arrObj.getString("delete-link");
                                    // arrObj.getString("continue-link");
                                    // arrObj.getString("pause-link");

                                }
                            } else {
                                //pri smazani nemam info o zprave, tak vracim vstupni zpravu
                                result = msg;
                            }

                        }
                    }
                }

            }

        } catch (Exception e) {
            Log.d("modifySendedMessage-err", e.toString());
            result = null;
        } finally {
            return result;
        }
    }

    public clsZprava deleteSendedMessage(clsZprava msg) {
        return modifySendedMessage(msg, "smaz");
    }

    public clsZprava archiveSendedMessage(clsZprava msg) {
        return modifySendedMessage(msg, "archivuj");
    }

    public clsZprava unarchiveSendedMessage(clsZprava msg) {
        return modifySendedMessage(msg, "odarchivuj");
    }

    public clsZprava checkQueueMessageState(clsZprava msg) {
        clsZprava result = null;
        JSONObject jObj;
        try {

            //tady musim byt prihlaseny - to si musim zajistit, driv nez zavolam tuto funkci
            ContentValues result_codes = new ContentValues();
            result_codes.put(null, "200");

            ContentValues parameters = new ContentValues();
            parameters.put("akce", "fronta");
            parameters.put("ukol", "stav-zpravy");
            parameters.put("id", msg.fronta_id);

            if (HttpPostRequest("", parameters, result_codes)) {
                jObj = new JSONObject(getResponse());

                if (jObj.length() != 0) {
                    if (jObj.has("response")) {
                        String resp = jObj.getString("response");
                        if (resp.equals(RESP_OK)) {

                            //odezva je v poradku, kdyz bylo vsechno OK, tak bych jeste mel mit data o zprave
                            if (jObj.has("data-array")) {
                                JSONArray jArr = jObj.getJSONArray("data-array");
                                JSONObject arrObj = jArr.getJSONObject(0);  //mela by tam byt jen jedna polozka

                                if (arrObj.has("state")) {  //kdyz je zprava ve fronte
                                    msg.stav = clsFunctions.int2stav(arrObj.getInt("state"));
                                    msg.fronta_id = arrObj.getInt("id");
                                }

                                if (arrObj.has("archived")) {   //kdyz je zprava uz odeslana
                                    msg.stav = clsStavZpravy.szSENDED;   //zprava je v odeslanych, takze uz je odeslaaana
                                    msg.archiv = arrObj.getInt("archived");
                                    msg.odeslano_id = arrObj.getInt("id");
                                }

                                msg.cislo = arrObj.getString("recipient-number");
                                msg.prezdivka = arrObj.getString("nickname");
                                msg.skript = arrObj.getString("script-name");
                                msg.sms = arrObj.getString("msg-text");
                                msg.datum = clsFunctions.str2date(arrObj.getString("date")/*, "dd.MM.yyyy HH:mm:ss"*/);

                                //vratim zpravu
                                result = msg;

                                /*
                                // pro zpravu ve fronte
                                arrObj.getInt("parts-cnt");
                                arrObj.getInt("parts-sended");
                                arrObj.getString("delete-link");
                                arrObj.getString("continue-link");
                                arrObj.getString("pause-link");
                                */

                                /*
                                // pro zpravu v odeslanych
                                arrObj.getString("delete-link");
                                arrObj.getString("archive-link");
                                arrObj.getString("unarchive-link");
                                */

                            }

                        }
                    }   //have response string
                }   //have some data form server
            } //resul cod is OK

        } catch (Exception e) {
            Log.d("checkMessageState-err", e.toString());
            result = null;
        } finally {
            return result;
        }
    }

}
