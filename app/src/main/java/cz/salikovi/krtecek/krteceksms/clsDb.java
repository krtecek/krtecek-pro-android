package cz.salikovi.krtecek.krteceksms;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by salik on 17. 7. 2015.
 */
public class clsDb extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "KrtekDB";

    private static final String TABLE_NAME_NASTAVENI = "nastaveni";
    private static final String TABLE_NAME_ZPRAVY = "zpravy";
    private static final String TABLE_NAME_SKRIPTY = "skripty";
    private static final String TABLE_NAME_KONTAKTY = "kontakty";

    private static final String KEY_ID = "id";
    private static final String KEY_UPD_TS = "upd_ts";
    //nastaveni
    private static final String KEY_NASTAVENI_NAZEV = "nazev";
    private static final String KEY_NASTAVENI_HODNOTA = "hodnota";
    //zpravy
    private static final String KEY_SMS_FRONTA_ID = "fronta_id";
    private static final String KEY_SMS_ODESLANO_ID = "odeslano_id";
    private static final String KEY_SMS_STAV_ID = "stav_id";
    private static final String KEY_SMS_DATUM = "datum";  //"yyyy-MM-dd HH:mm:ss"
    private static final String KEY_SMS_KONTAKT_ID = "kontakt_id";
    private static final String KEY_SMS_PREZDIVKA = "prezdivka";
    private static final String KEY_SMS_CISLO = "cislo";
    private static final String KEY_SMS_SKRIPT = "skript";
    private static final String KEY_SMS_SKRIPT_ID = "skript_id";
    private static final String KEY_SMS_TEXT = "sms";
    private static final String KEY_SMS_ARCHIV = "archiv";
    //skripty
    private static final String KEY_SKRIPTY_NAZEV = "nazev";
    private static final String KEY_SKRIPTY_KRT_SCRIPT_ID = "krtek_skript_id";
    private static final String KEY_SKRIPTY_IMG = "obrazek";
    //kontakty
    private static final String KEY_KONTAKTY_PREZDIVKA = "prezdivka";
    private static final String KEY_KONTAKTY_CISLO = "cislo";
    private static final String KEY_KONTAKTY_KRT_CONTACT_ID = "krtek_kontakt_id";
    private static final String KEY_KONTAKTY_KRT_SCRIPT_ID = "krtek_skript_id";
    private static final String KEY_KONTAKTY_AVATAR = "avatar";

    private static final String[] COLUMNS_NASTAVENI = { KEY_ID, KEY_NASTAVENI_NAZEV, KEY_NASTAVENI_HODNOTA };
    private static final String[] COLUMNS_SMS = { KEY_ID, KEY_SMS_FRONTA_ID, KEY_SMS_ODESLANO_ID, KEY_SMS_STAV_ID,
            KEY_SMS_DATUM, KEY_SMS_KONTAKT_ID, KEY_SMS_PREZDIVKA, KEY_SMS_CISLO, KEY_SMS_SKRIPT,
            KEY_SMS_SKRIPT_ID, KEY_SMS_TEXT, KEY_SMS_ARCHIV };
    private static final String[] COLUMNS_SKRIPTY = { KEY_ID, KEY_UPD_TS, KEY_SKRIPTY_NAZEV, KEY_SKRIPTY_KRT_SCRIPT_ID, KEY_SKRIPTY_IMG};
    private static final String[] COLUMNS_KONTAKTY = { KEY_ID, KEY_UPD_TS, KEY_KONTAKTY_PREZDIVKA, KEY_KONTAKTY_CISLO,
            KEY_KONTAKTY_KRT_CONTACT_ID, KEY_KONTAKTY_KRT_SCRIPT_ID, KEY_KONTAKTY_AVATAR};

    public clsDb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create tables

        String CREATE_SETTINGS_TABLE = "CREATE TABLE " + TABLE_NAME_NASTAVENI + " ( " +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_NASTAVENI_NAZEV + " TEXT, " +
                    KEY_NASTAVENI_HODNOTA + " TEXT"
                + ")";
        db.execSQL(CREATE_SETTINGS_TABLE);

        String CREATE_MESSAGES_TABLE = "CREATE TABLE " + TABLE_NAME_ZPRAVY + " ( " +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_SMS_FRONTA_ID + " INTEGER, " +
                    KEY_SMS_ODESLANO_ID + " INTEGER, " +
                    KEY_SMS_DATUM + " DATETIME, " +
                    KEY_SMS_STAV_ID + " INTEGER, " +
                    KEY_SMS_KONTAKT_ID + " INTEGER, " +
                    KEY_SMS_PREZDIVKA + " TEXT, " +
                    KEY_SMS_CISLO + " TEXT, " +
                    KEY_SMS_SKRIPT + " TEXT, " +
                    KEY_SMS_SKRIPT_ID + " INTEGER, " +
                    KEY_SMS_ARCHIV + " INTEGER, " +
                    KEY_SMS_TEXT + " TEXT"
                + ")";
        db.execSQL(CREATE_MESSAGES_TABLE);

        String CREATE_SCRIPTS_TABLE = "CREATE TABLE " + TABLE_NAME_SKRIPTY + " ( " +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_SKRIPTY_NAZEV + " TEXT, " +
                KEY_SKRIPTY_KRT_SCRIPT_ID + " INTEGER, " +
                KEY_SKRIPTY_IMG + " BLOB, " +
                KEY_UPD_TS + " DATETIME"
                + ")";
        db.execSQL(CREATE_SCRIPTS_TABLE);

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_NAME_KONTAKTY + " ( " +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_KONTAKTY_PREZDIVKA + " TEXT, " +
                KEY_KONTAKTY_CISLO + " TEXT, " +
                KEY_KONTAKTY_KRT_CONTACT_ID + " INTEGER, " +
                KEY_KONTAKTY_KRT_SCRIPT_ID + " INTEGER, " +
                KEY_KONTAKTY_AVATAR + " BLOB, " +
                KEY_UPD_TS + " DATETIME"
                + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Drop old tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_NASTAVENI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ZPRAVY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SKRIPTY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_KONTAKTY);

        // create fresh tables
        this.onCreate(db);
    }

    private clsNastaveni readSettings(String name, String value) {
        SQLiteDatabase db = this.getReadableDatabase();
        clsNastaveni nast = new clsNastaveni();
        Cursor cursor = null;

        try {
            nast.hodnota = value;

            cursor = db.query(TABLE_NAME_NASTAVENI,
                    COLUMNS_NASTAVENI, // b. column names
                    "(" + KEY_NASTAVENI_NAZEV + " = ?)", // c. where
                    new String[] { name }, // d. where args
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null); // h. limit

            if (cursor != null) {
                cursor.moveToFirst();

                if (cursor.getCount() == 1) {
                    //doplnuju
                    nast.id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    nast.nazev = cursor.getString(cursor.getColumnIndex(KEY_NASTAVENI_NAZEV));
                    nast.hodnota = cursor.getString(cursor.getColumnIndex(KEY_NASTAVENI_HODNOTA));
                }
            }
        } catch (Exception e) {
            Log.d("readSettings(" + name + ") - Error", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        Log.d("readSettings(" + name + ")", nast.toString());
        return nast;
    }

    public String readStrSettings(String name) {
        return readStrSettings(name, "");
    }
    public String readStrSettings(String name, String value) {
        clsNastaveni nast = readSettings(name, value);
        Log.d("readStrSettings(" + name + ")", nast.toString());
        return nast.hodnota;
    }

    public int readIntSettings(String name) {
        return readIntSettings(name, -1);
    }
    public int readIntSettings(String name, int value) {
        clsNastaveni nast = readSettings(name, Integer.toString(value));
        Log.d("readIntSettings(" + name + ")", nast.toString());
        return Integer.parseInt(nast.hodnota);
    }

    public boolean readBoolSettings(String name) {
        return readBoolSettings(name, false);
    }
    public boolean readBoolSettings(String name, boolean value) {
        clsNastaveni nast = readSettings(name, Boolean.toString(value));
        Log.d("readBoolSettings(" + name + ")", nast.toString());
        return Boolean.parseBoolean(nast.hodnota);
    }

    private boolean writeSettings(String name, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        clsNastaveni nast = new clsNastaveni();
        boolean result = false;

        nast.nazev = name;
        nast.hodnota = value;

        try {
            cursor = db.query(TABLE_NAME_NASTAVENI,
                    COLUMNS_NASTAVENI, // b. column names
                    "(" + KEY_NASTAVENI_NAZEV + " = ?)", // c. where
                    new String[]{name}, // d. where args
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null); // h. limit

            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.getCount() == 0) {
                    //create
                    ContentValues values = new ContentValues();
                    values.put(KEY_NASTAVENI_NAZEV, nast.nazev);
                    values.put(KEY_NASTAVENI_HODNOTA, nast.hodnota);

                    nast.id = (int) db.insert(TABLE_NAME_NASTAVENI,
                            null, //nullColumnHack
                            values);

                    result = (nast.id != -1);
                } else {
                    //update
                    nast.id = cursor.getInt(cursor.getColumnIndex(KEY_ID));

                    ContentValues values = new ContentValues();
                    values.put(KEY_NASTAVENI_NAZEV, nast.nazev);
                    values.put(KEY_NASTAVENI_HODNOTA, nast.hodnota);

                    int affected = db.update(TABLE_NAME_NASTAVENI,
                            values, // column/value
                            "(" + KEY_ID + " = ?)", // where
                            new String[]{nast.id.toString()}); //where args

                    result = (affected == 1);
                }
            } //cursor neni null

        } catch (Exception e) {
            result = false;
            Log.d("WriteSettings - Error", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        Log.d("WriteSettings(" + name + ")", nast.toString());
        return result;
    }

    public boolean writeStrSettings(String name, String value) {
        Log.d("writeStrSettings(" + name + ")", value);
        return writeSettings(name, value);
    }

    public boolean writeIntSettings(String name, int value) {
        Log.d("writeIntSettings(" + name + ")", Integer.toString(value));
        return writeSettings(name, Integer.toString(value));
    }

    public boolean writeBoolSettings(String name, boolean value) {
        Log.d("writeBoolSettings(" + name + ")", Boolean.toString(value));
        return writeSettings(name, Boolean.toString(value));
    }

    public int addMessage(clsZprava zprava) {
        int result = -1;

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_SMS_ARCHIV, zprava.archiv);
            values.put(KEY_SMS_CISLO, zprava.cislo);
            values.put(KEY_SMS_DATUM, zprava.getDatum("yyyy-MM-dd HH:mm:ss"));
            values.put(KEY_SMS_FRONTA_ID, zprava.fronta_id);
            values.put(KEY_SMS_ODESLANO_ID, zprava.odeslano_id);
            values.put(KEY_SMS_KONTAKT_ID, zprava.kontakt_id);
            values.put(KEY_SMS_PREZDIVKA, zprava.prezdivka);
            values.put(KEY_SMS_SKRIPT, zprava.skript);
            values.put(KEY_SMS_SKRIPT_ID, zprava.skript_id);
            values.put(KEY_SMS_STAV_ID, clsFunctions.stav2int(zprava.stav));
            values.put(KEY_SMS_TEXT, zprava.sms);

            zprava.id = (int) db.insert(TABLE_NAME_ZPRAVY,
                    null, //nullColumnHack
                    values);

            result = zprava.id;

        } catch (Exception e) {
            result = -1;
        } finally {
            Log.d("AddMessage", zprava.toString());
            db.close();
        }

        return result;
    }

    public int updMessage(clsZprava zprava) {
        int result = -1;

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            ContentValues values = new ContentValues();
            values.put(KEY_SMS_ARCHIV, zprava.archiv);
            values.put(KEY_SMS_CISLO, zprava.cislo);
            values.put(KEY_SMS_DATUM, zprava.getDatum("yyyy-MM-dd HH:mm:ss"));
            values.put(KEY_SMS_FRONTA_ID, zprava.fronta_id);
            values.put(KEY_SMS_ODESLANO_ID, zprava.odeslano_id);
            values.put(KEY_SMS_KONTAKT_ID, zprava.kontakt_id);
            values.put(KEY_SMS_PREZDIVKA, zprava.prezdivka);
            values.put(KEY_SMS_SKRIPT, zprava.skript);
            values.put(KEY_SMS_SKRIPT_ID, zprava.skript_id);
            values.put(KEY_SMS_STAV_ID, clsFunctions.stav2int(zprava.stav));
            values.put(KEY_SMS_TEXT, zprava.sms);

            int affected = db.update(TABLE_NAME_ZPRAVY,
                    values, // column/value
                    "(" + KEY_ID + " = ?)", // where
                    new String[]{Integer.toString(zprava.id)}); //where args

            if (affected == 1) {
                result = zprava.id;
            }

        } catch (Exception e) {
            result = -1;
        } finally {
            Log.d("UpdateMessage", zprava.toString());
            db.close();
        }

        return result;
    }

    public boolean delMessage(clsZprava zprava) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            int affected = db.delete(TABLE_NAME_ZPRAVY,
                    "(" + KEY_ID + " = ?)",  // where
                    new String[]{Integer.toString(zprava.id)}); //where args

            result = (affected == 1);

        } catch (Exception e) {
            Log.d("DeleteMessage-error", e.toString());
            result = false;
        } finally {
            Log.d("DeleteMessage", zprava.toString());
            db.close();
        }
        return result;
    }

    public boolean delMessage(int id) {
        clsZprava zprava = new clsZprava();
        zprava.id = id;
        return delMessage(zprava);
    }

    public int getLastFrontaIdFronta() {
        int result = -1;
        try {

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = null;

            try {

                String where = "(" + KEY_SMS_ODESLANO_ID + " != ?)";
                String[] where_args = { "-1" };

                cursor = db.query(TABLE_NAME_ZPRAVY,
                        new String[] { "max(" + KEY_SMS_FRONTA_ID + ") as last_id" }, // b. column names
                        "(" + KEY_SMS_ODESLANO_ID + " = ?)", // c. where
                        new String[] { Integer.toString(-1) }, // d. where args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

                if (cursor != null) {
                    cursor.moveToFirst();

                    if (cursor.getCount() == 1) {
                        result = cursor.getInt(cursor.getColumnIndex("last_id"));
                        //kdyz je maximum "0", zamena to, ze namam jeste zadnou zpravu => ID=-1
                        if (result == 0) {
                            result = -1;
                        }
                    }
                }
            } catch (Exception e) {
                Log.d("getLastFrontaIdFronta-E", e.toString());
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                db.close();
            }

        } catch (Exception e) {
            result = -1;
            Log.d("getLastFrontaIdFronta-E", e.toString());
        } finally {
            Log.d("getLastFrontaIdFronta", Integer.toString(result));
            return result;
        }
    }

    public int getLastFrontaIdOdeslano() {
        int result = -1;
        try {

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = null;
            try {

                cursor = db.query(TABLE_NAME_ZPRAVY,
                        new String[] { "max(" + KEY_SMS_FRONTA_ID + ") as last_id" }, // b. column names
                        "(" + KEY_SMS_ODESLANO_ID + " != ?)", // c. where
                        new String[] { Integer.toString(-1) }, // d. where args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

                if (cursor != null) {
                    cursor.moveToFirst();

                    if (cursor.getCount() == 1) {
                        result = cursor.getInt(cursor.getColumnIndex("last_id"));
                        //kdyz je maximum "0", zamena to, ze namam jeste zadnou zpravu => ID=-1
                        if (result == 0) {
                            result = -1;
                        }
                    }
                }
            } catch (Exception e) {
                Log.d("getLastFrontaIdOdes-Err", e.toString());
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                db.close();
            }

        } catch (Exception e) {
            result = -1;
            Log.d("getLastFrontaIdOdes-Err", e.toString());
        } finally {
            Log.d("getLastFrontaIdOdeslano", Integer.toString(result));
            return result;
        }
    }

    public clsZprava getMessage(int fronta_id, int odeslano_id, int db_id) {
        clsZprava result = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        try {

            String where = "";
            List<String> whereList = new ArrayList<String>();

            if (fronta_id != -1) {
                if (where != "") {
                    where = where + " and ";
                }
                where = where + "(" + KEY_SMS_FRONTA_ID + " = ?)";
                whereList.add( Integer.toString(fronta_id) );
            }
            if (odeslano_id != -1) {
                if (where != "") {
                    where = where + " and ";
                }
                where = where + "(" + KEY_SMS_ODESLANO_ID + " = ?)";
                whereList.add( Integer.toString(odeslano_id) );
            }
            if (db_id != -1) {
                if (where != "") {
                    where = where + " and ";
                }
                where = where + "(" + KEY_ID + " = ?)";
                whereList.add( Integer.toString(db_id) );
            }

            cursor = db.query(TABLE_NAME_ZPRAVY,
                    COLUMNS_SMS, // b. column names
                    where,
                    whereList.toArray(new String[0]),
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null); // h. limit

            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.getCount() == 1) {

                    result = new clsZprava(
                            cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                            cursor.getInt(cursor.getColumnIndex(KEY_SMS_FRONTA_ID)),
                            cursor.getInt(cursor.getColumnIndex(KEY_SMS_ODESLANO_ID)),
                            clsFunctions.int2stav(cursor.getInt(cursor.getColumnIndex(KEY_SMS_STAV_ID))),
                            clsFunctions.str2date(cursor.getString(cursor.getColumnIndex(KEY_SMS_DATUM))),
                            cursor.getInt(cursor.getColumnIndex(KEY_SMS_KONTAKT_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_SMS_PREZDIVKA)),
                            cursor.getString(cursor.getColumnIndex(KEY_SMS_CISLO)),
                            cursor.getString(cursor.getColumnIndex(KEY_SMS_SKRIPT)),
                            cursor.getInt(cursor.getColumnIndex(KEY_SMS_SKRIPT_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_SMS_TEXT)),
                            cursor.getInt(cursor.getColumnIndex(KEY_SMS_ARCHIV))
                    );

                }
            }

        } catch (Exception e) {
            result = null;
            Log.d("GetMessage - Error", e.toString());
        } finally {
            if (result == null) {
                Log.d("GetMessage", "null");
            } else {
                Log.d("GetMessage", result.toString());
            }
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public clsZprava getQueueMessage(int fronta_id) {
        return getMessage(fronta_id, -1, -1);
    }

    public clsZprava getSendedMessage(int odeslano_id) {
        return getMessage(-1, odeslano_id, -1);
    }

    public clsZprava getDbMessage(int db_id) {
        return getMessage(-1, -1, db_id);
    }

    public int zpracujZpravu(clsZprava zprava) {
        int result = -1;
        try {

            clsZprava msg = getMessage(zprava.fronta_id, zprava.odeslano_id, zprava.id);
            if (msg == null) {
                //insert (return DbID if all is ok)
                result = addMessage(zprava);
            } else {
                //update (return DbID if all is ok)
                zprava.id = msg.id;
                result = updMessage(zprava);
            }

        } catch (Exception e) {
            result = -1;
            Log.d("aktualizujZpravu-Error", e.toString());
        } finally {
            Log.d("aktualizujZpravu", zprava.toString());
            return result;
        }
    }

    public List<clsZprava> getQueueMessages() {
        List<clsZprava> result = new ArrayList<clsZprava>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            String where = "";
            String order = "";
            List<String> whereList = new ArrayList<String>();

            where = "(" + KEY_SMS_ODESLANO_ID + " = ?)";
            order = KEY_SMS_DATUM + " ASC";
            whereList.add(Integer.toString( -1 ));

            cursor = db.query(TABLE_NAME_ZPRAVY,
                    COLUMNS_SMS, // b. column names
                    where,
                    whereList.toArray(new String[0]),
                    null, // e. group by
                    null, // f. having
                    order, // g. order by
                    null); // h. limit

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        clsZprava msg = new clsZprava(
                                cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_FRONTA_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_ODESLANO_ID)),
                                clsFunctions.int2stav(cursor.getInt(cursor.getColumnIndex(KEY_SMS_STAV_ID))),
                                clsFunctions.str2date(cursor.getString(cursor.getColumnIndex(KEY_SMS_DATUM))),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_KONTAKT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_PREZDIVKA)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_CISLO)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_SKRIPT)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_SKRIPT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_TEXT)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_ARCHIV))
                        );
                        result.add(msg);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result.clear();
            Log.d("getQueueMessages-Error", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public List<clsZprava> getSendedMessages() {
        List<clsZprava> result = new ArrayList<clsZprava>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {

            String where = "";
            String order = "";
            List<String> whereList = new ArrayList<String>();

            where = "(" + KEY_SMS_ODESLANO_ID + " != ?)";
            order = KEY_SMS_DATUM + " DESC";
            whereList.add(Integer.toString( -1 ));

            cursor = db.query(TABLE_NAME_ZPRAVY,
                    COLUMNS_SMS, // b. column names
                    where,
                    whereList.toArray(new String[0]),
                    null, // e. group by
                    null, // f. having
                    order, // g. order by
                    null); // h. limit

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        clsZprava msg = new clsZprava(
                                cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_FRONTA_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_ODESLANO_ID)),
                                clsFunctions.int2stav(cursor.getInt(cursor.getColumnIndex(KEY_SMS_STAV_ID))),
                                clsFunctions.str2date(cursor.getString(cursor.getColumnIndex(KEY_SMS_DATUM))),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_KONTAKT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_PREZDIVKA)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_CISLO)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_SKRIPT)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_SKRIPT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_TEXT)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_ARCHIV))
                        );
                        result.add(msg);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result.clear();
            Log.d("getSendedMessages-Error", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public boolean delOldQueueMessages() {
        boolean result = false;
        int affected = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            //mazu zpravy, ktere maji nejake "fronta_id", ty uz jsou na serveru
            affected = db.delete(TABLE_NAME_ZPRAVY,
                    "(" + KEY_SMS_FRONTA_ID + " != ?) and (" + KEY_SMS_ODESLANO_ID + " = ?)",  // where
                    new String[] { Integer.toString(-1), Integer.toString(-1) } //where args
                );

            result = true;

        } catch (Exception e) {
            Log.d("delOldQueueMessages-err", e.toString());
            result = false;
        } finally {
            Log.d("delOldQueueMessages", "vymazano: " + Integer.toString(affected) );
            db.close();
        }
        return result;
    }

    public boolean delAllHistoryMessages() {
        boolean result = false;
        int affected = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            //mazu zpravy, ktere maji nejake "odeslano_id", ty uz jsou v historii
            affected = db.delete(TABLE_NAME_ZPRAVY,
                    "(" + KEY_SMS_ODESLANO_ID + " != ?)",  // where
                    new String[] { Integer.toString(-1) }  //where args
            );

            result = true;

        } catch (Exception e) {
            Log.d("delAllHistoryMsgs-err", e.toString());
            result = false;
        } finally {
            Log.d("delAllHistoryMsgs", "vymazano: " + Integer.toString(affected) );
            db.close();
        }
        return result;
    }

    public List<clsZprava> getUnsendedMessages() {
        List<clsZprava> result = new ArrayList<clsZprava>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {

            String where = "";
            List<String> whereList = new ArrayList<String>();

            //neodeslane zpravy nemaji FRONTA_ID
            where = "(" + KEY_SMS_FRONTA_ID + " == ?)";
            whereList.add(Integer.toString( -1 ));

            cursor = db.query(TABLE_NAME_ZPRAVY,
                    COLUMNS_SMS, // b. column names
                    where,
                    whereList.toArray(new String[0]),
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null); // h. limit

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        clsZprava msg = new clsZprava(
                                cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_FRONTA_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_ODESLANO_ID)),
                                clsFunctions.int2stav(cursor.getInt(cursor.getColumnIndex(KEY_SMS_STAV_ID))),
                                clsFunctions.str2date(cursor.getString(cursor.getColumnIndex(KEY_SMS_DATUM))),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_KONTAKT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_PREZDIVKA)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_CISLO)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_SKRIPT)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_SKRIPT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SMS_TEXT)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SMS_ARCHIV))
                        );
                        result.add(msg);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result.clear();
            Log.d("getUnsendedMessages-Err", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public boolean readContacts(List<clsKontakt> contacts) {
        boolean result = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Bitmap bmp = null;
        byte[] decodedString;
        Cursor cursor = null;

        try {

            cursor = db.query(TABLE_NAME_KONTAKTY,
                    COLUMNS_KONTAKTY, // b. column names
                    null, // c. where condition
                    null, // d. where params
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null); // h. limit

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        bmp = clsFunctions.blob2Bitmap(cursor.getBlob(cursor.getColumnIndex(KEY_KONTAKTY_AVATAR)));

                        clsKontakt cnt = new clsKontakt(
                                //cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_KONTAKTY_KRT_CONTACT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_KONTAKTY_PREZDIVKA)),
                                cursor.getString(cursor.getColumnIndex(KEY_KONTAKTY_CISLO)),
                                cursor.getInt(cursor.getColumnIndex(KEY_KONTAKTY_KRT_SCRIPT_ID)),
                                bmp,
                                cursor.getString(cursor.getColumnIndex(KEY_UPD_TS))
                        );
                        contacts.add(cnt);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result = false;
            Log.d("readContacts", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public boolean readScripts(List<clsSkript> scripts) {
        boolean result = false;
        SQLiteDatabase db = this.getReadableDatabase();
        Bitmap bmp = null;
        byte[] decodedString;
        Cursor cursor = null;

        try {

            cursor = db.query(TABLE_NAME_SKRIPTY,
                    COLUMNS_SKRIPTY, // b. column names
                    null, // c. where condition
                    null, // d. where params
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null); // h. limit

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        bmp = clsFunctions.blob2Bitmap( cursor.getBlob(cursor.getColumnIndex(KEY_SKRIPTY_IMG)) );

                        clsSkript scr = new clsSkript(
                                //cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                                cursor.getInt(cursor.getColumnIndex(KEY_SKRIPTY_KRT_SCRIPT_ID)),
                                cursor.getString(cursor.getColumnIndex(KEY_SKRIPTY_NAZEV)),
                                bmp,
                                cursor.getString(cursor.getColumnIndex(KEY_UPD_TS))
                        );
                        scripts.add(scr);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result = false;
            Log.d("readScripts", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public String getLastContactChangeDateTimeStr() {
        String result = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = db.rawQuery("select strftime('%Y-%m-%d %H:%M:%S', max(upd_ts)) as upd_ts from " + TABLE_NAME_KONTAKTY, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do { //radek by mel byt jen jeden ...
                        result = cursor.getString(cursor.getColumnIndex("upd_ts"));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result = "";
            Log.d("getLstCntChngDTStr", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public String getLastScriptChangeDateTimeStr() {
        String result = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = db.rawQuery("select strftime('%Y-%m-%d %H:%M:%S', max(upd_ts)) as upd_ts from " + TABLE_NAME_SKRIPTY, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do { //radek by mel byt jen jeden ...
                        result = cursor.getString(cursor.getColumnIndex("upd_ts"));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result = "";
            Log.d("getLstScrChngDtTmStr", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public int getScriptCnt() {
        int result = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = db.rawQuery("select count(*) as pocet from " + TABLE_NAME_SKRIPTY, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do { //radek by mel byt jen jeden ...
                        result = cursor.getInt(cursor.getColumnIndex("pocet"));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result = 0;
            Log.d("getLastScriptCnt", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public int getContactCnt() {
        int result = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = db.rawQuery("select count(*) as pocet from " + TABLE_NAME_KONTAKTY, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do { //radek by mel byt jen jeden ...
                        result = cursor.getInt(cursor.getColumnIndex("pocet"));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            result = 0;
            Log.d("getContactCnt", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
            return result;
        }
    }

    public boolean deleteAllContacts() {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        try {

            db.execSQL("delete from " + TABLE_NAME_KONTAKTY);
            result = true;

        } catch (Exception e) {
            result = false;
            Log.d("deleteAllContacts", e.toString());
        } finally {
            db.close();
            return result;
        }
    }

    public boolean deleteAllScripts() {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        try {

            db.execSQL("delete from " + TABLE_NAME_SKRIPTY);
            result = true;

        } catch (Exception e) {
            result = false;
            Log.d("deleteAllScripts", e.toString());
        } finally {
            db.close();
            return result;
        }
    }

    public boolean updateContacts(List<clsKontakt> con) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        clsKontakt k;
        int affected;
        long rowid;
        ContentValues values = new ContentValues();

        try {

            if (con.size() > 0) {
                Iterator iter = con.iterator();
                while (iter.hasNext()) {
                    k = (clsKontakt) iter.next();
                    // -----

                    values.clear();
                    values.put(KEY_UPD_TS, k.updTsStr);
                    values.put(KEY_KONTAKTY_KRT_SCRIPT_ID, k.SrvScrDbId);
                    values.put(KEY_KONTAKTY_PREZDIVKA, k.Prezdivka);
                    values.put(KEY_KONTAKTY_CISLO, k.Cislo);
                    values.put(KEY_KONTAKTY_AVATAR, clsFunctions.bitmap2blob(k.Avatar));


                    affected = db.update(TABLE_NAME_KONTAKTY,
                            values, // column/value
                            "(" + KEY_KONTAKTY_KRT_CONTACT_ID + " = ?)", // where
                            new String[]{k.SrvDbId.toString()}); //where args

                    //result = (affected == 1);
                    if (affected == 0) {
                        values.put(KEY_KONTAKTY_KRT_CONTACT_ID, k.SrvDbId.toString());
                        rowid = db.insert(TABLE_NAME_KONTAKTY,
                                null,
                                values ); // column/value
                    }

                    // -----
                }
            }

            result = true;
        } catch (Exception e) {
            result = false;
            Log.d("updateContacts", e.toString());
        } finally {
            db.close();
            return result;
        }
    }

    public boolean updateScripts(List<clsSkript> scr) {
        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        clsSkript s;
        int affected;
        long rowid;
        ContentValues values = new ContentValues();

        try {

            if (scr.size() > 0) {
                Iterator iter = scr.iterator();
                while (iter.hasNext()) {
                    s = (clsSkript) iter.next();
                    // -----

                    values.clear();
                    values.put(KEY_UPD_TS, s.updTsStr);
                    values.put(KEY_SKRIPTY_NAZEV, s.Nazev);
                    values.put(KEY_SKRIPTY_IMG, clsFunctions.bitmap2blob(s.Img));

                    affected = db.update(TABLE_NAME_SKRIPTY,
                            values, // column/value
                            "(" + KEY_SKRIPTY_KRT_SCRIPT_ID + " = ?)", // where
                            new String[]{s.SrvDbId.toString()} ); //where args

                    //result = (affected == 1);
                    if (affected == 0) {
                        values.put(KEY_SKRIPTY_KRT_SCRIPT_ID, s.SrvDbId.toString());
                        rowid = db.insert(TABLE_NAME_SKRIPTY,
                                null,
                                values ); // column/value
                    }

                    // -----
                }
            }

            result = true;
        } catch (Exception e) {
            result = false;
            Log.d("updateScripts", e.toString());
        } finally {
            db.close();
            return result;
        }
    }

}
