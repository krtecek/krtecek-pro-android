package cz.salikovi.krtecek.krteceksms;

import android.util.Log;

import java.util.TimerTask;

/**
 * Created by salik on 7. 8. 2015.
 */
public class clsQueueTimerTask extends TimerTask {
    private QueueFragment frag = null;

    public clsQueueTimerTask(QueueFragment f) {
        frag = f;
    }

    @Override
    public void run() {
        try {
            frag.getMyAct().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    frag.CheckQueueChanges();
                }

            });
        } catch (Exception e) {
            Log.e("clsQueueTimerTask", e.toString());
        }
    }
}
