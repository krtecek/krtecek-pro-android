package cz.salikovi.krtecek.krteceksms;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salik on 17. 3. 2016.
 */
public class clsSkriptyAdapter extends BaseAdapter {

    private Activity act;
    private List<clsSkript> data;
    private LayoutInflater inflater = null;

    public Resources res;

    public clsSkriptyAdapter (Activity a, ArrayList d, Resources r) {
        act = a;
        data = d;
        res = r;

        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //trida pro radek ListView
    public static class ViewHolder{
        public ImageView skriptImg;
        public TextView skriptNazev;
    }

    @Override
    public int getCount() {
        if (data.size() <= 0) {
            return 0;
        } else {
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        clsSkript scr = null;
        try {
            scr = data.get(position);
        } catch (Exception e) {
            scr = null;
        } finally {
            return scr;
        }
    }

    @Override
    public long getItemId(int position) {
        int id = -1;
        try {
            clsSkript scr = data.get(position);
            id = scr.SrvDbId;
        } catch (Exception e) {
            id  = -1;
        } finally {
            return id;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, false);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, true);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent, boolean dropDown) {
        View row = convertView;
        ViewHolder holder;
        clsSkript scr = null;

        final String nameSpace = "   ";
        String space = "";

        if (row == null) {
            if (dropDown) {
                row = inflater.inflate(R.layout.skript_combo_item_drop_down, null);
            } else {
                row = inflater.inflate(R.layout.skript_combo_item, null);
            }

            holder = new ViewHolder();
            holder.skriptImg = (ImageView) row.findViewById(R.id.imageViewSkriptImg);
            holder.skriptNazev = (TextView) row.findViewById(R.id.textViewSkriptNazev);

            row.setTag( holder );    //set holder with LayoutInflater
        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (data.size() <= 0) {
            holder.skriptImg.setImageResource(android.R.color.transparent);
            holder.skriptNazev.setText("Žádná data");
        } else {
            scr = data.get(position);
            if (scr.Img != null) {
                space = nameSpace;
                holder.skriptImg.setImageBitmap(scr.Img);
            } else {
                space = "";
                holder.skriptImg.setImageResource(android.R.color.transparent);
            }
            holder.skriptNazev.setText(space + scr.toString());
        }

        return row;
    }
}
