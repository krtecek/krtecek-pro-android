package cz.salikovi.krtecek.krteceksms;

import android.text.TextUtils;

import java.net.CookieManager;
import java.net.HttpCookie;
import java.util.List;

/**
 * Created by salik on 24. 7. 2015.
 */
public class clsHttpCookies extends CookieManager {

    public boolean clear() {
        boolean result = false;
        try {
            this.getCookieStore().getCookies().clear();
            result = true;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public boolean add(String name, String value) {
        boolean result = false;
        try {
            HttpCookie cookie = new HttpCookie(name, value);
            this.getCookieStore().add(null, cookie);
            result = true;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public boolean set(String name, String value) {
        clear();
        return add(name, value);
    }

    public String getFormatedCookies(String separator) {
        String result = "";
        try {
            result = TextUtils.join(separator, this.getCookieStore().getCookies());
        } catch (Exception e) {
            result = "";
        }
        return result;
    }

    public String getFormatedCookies() {
        return getFormatedCookies(";");
    }

    public boolean loadFromHeader(clsHttpHeader header) {
        boolean result = false;
        clear();

        try {
            if (header != null) {

                List<String> cookies_resp = header.data.get("Set-Cookie");
                if (cookies_resp != null) {
                    for (String cookie : cookies_resp) {
                        this.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                    }
                }

            }
            result = true;
        } catch (Exception e) {
            result = false;
        }

        return result;
    }
}
