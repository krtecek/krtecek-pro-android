package cz.salikovi.krtecek.krteceksms;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by salik on 29. 7. 2015.
 */
public class clsHistorieAdapter extends BaseAdapter {

    private Activity act;
    private ArrayList data;
    private static LayoutInflater inflater = null;

    public Resources res;

    clsZprava tmpMsg = null;
    int i = 0;

    public clsHistorieAdapter(Activity a, ArrayList d, Resources r) {
        act = a;
        data = d;
        res = r;

        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (data.size() <= 0) {
            return 0;
        } else {
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        clsZprava msg = null;
        try {
            msg = (clsZprava) data.get(position);
        } catch (Exception e) {
            msg = null;
        } finally {
            return msg;
        }
    }

    @Override
    public long getItemId(int position) {
        int id = -1;
        try {
            clsZprava msg = (clsZprava) data.get(position);
            id = msg.id;
        } catch (Exception e) {
            id  = -1;
        } finally {
            return id;
        }
    }

    //trida pro radek ListView
    public static class ViewHolder{
        public ImageView stavImg;
        public TextView stav;
        public TextView prijemce;
        public TextView datum;
        public TextView sms;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.historie_list_item, null);

            holder = new ViewHolder();
            holder.stavImg = (ImageView) vi.findViewById(R.id.imageViewStav);
            holder.stav = (TextView) vi.findViewById(R.id.textViewStav);
            holder.prijemce = (TextView) vi.findViewById(R.id.textViewPrijemce);
            holder.datum = (TextView) vi.findViewById(R.id.textViewDatum);
            holder.sms = (TextView) vi.findViewById(R.id.textViewSms);

            vi.setTag( holder );    //set holder with LayoutInflater

        } else {
            holder = (ViewHolder) vi.getTag();
        }

        if (data.size() <= 0) {
            holder.sms.setText("Žádná data");
        } else {
            tmpMsg = null;
            tmpMsg = (clsZprava) data.get(position);

            if (tmpMsg.archiv == 1) {
                holder.stavImg.setBackgroundResource(R.drawable.archive_icon_16);
                holder.stav.setText( "archivována" );
            } else {
                holder.stavImg.setBackgroundResource(clsFunctions.ResImgStavu(tmpMsg.stav));
                holder.stav.setText(clsFunctions.NazevStavu(tmpMsg.stav));
            }

            //titulek menu
            String prijemce = tmpMsg.prezdivka.trim();
            if (prijemce.length() > 0) {
                prijemce = prijemce + " (" + tmpMsg.cislo + ")";
            } else {
                prijemce = tmpMsg.cislo;
            }

            holder.prijemce.setText( prijemce );
            holder.datum.setText( tmpMsg.getDatum() );
            holder.sms.setText( tmpMsg.sms );
        }

        return vi;
    }

}
