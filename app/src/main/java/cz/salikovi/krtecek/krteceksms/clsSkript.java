package cz.salikovi.krtecek.krteceksms;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by salik on 14. 7. 2015.
 */
public class clsSkript {

    public Integer SrvDbId;
    public String Nazev;
    public Bitmap Img;
    public String updTsStr;

    public clsSkript() {
        SrvDbId = -1;
        Nazev = "";
        Img = null;
        updTsStr = "";
    }

    public clsSkript(Integer aSrvDbId, String aNazev, Bitmap aImg, String aUpdTsStr) {
        SrvDbId = aSrvDbId;
        Nazev = aNazev;
        Img = aImg;
        updTsStr = aUpdTsStr;
    }

    @Override
    public String toString() {
        return String.format("%s", this.Nazev);
    }

}
