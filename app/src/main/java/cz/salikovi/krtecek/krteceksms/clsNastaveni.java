package cz.salikovi.krtecek.krteceksms;

/**
 * Created by salik on 20. 7. 2015.
 */
public class clsNastaveni {

    public Integer id;
    public String nazev;
    public String hodnota;

    public clsNastaveni() {
        id = -1;
        nazev = "";
        hodnota = "";
    }

    @Override
    public String toString() {
        return "Nastaveni [id=" + id
                + ", nazev=" + nazev
                + ", hodnota=" + hodnota
                + "]";
    }

}
